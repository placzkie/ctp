package pl.edu.agh.ctp.users;

import com.google.common.collect.Lists;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Leszek Placzkiewicz on 2016-07-30.
 */
public class UserRepositoryLocalImpl implements IUserRepository {

    private final Set<UserCredentials> users;

    private final Map<String, String> userConferences;


    public UserRepositoryLocalImpl() {
        this.users = populateUsers();
        this.userConferences = populateUserConferences();
    }

    @Override
    public boolean authenticateUser(UserCredentials userCredentials) {
        Optional<UserCredentials> userOptional =
                users.stream().filter(uc -> uc.getUsername().equals(userCredentials.getUsername())).findAny();
        if (!userOptional.isPresent()) {
            return false;
        }
        UserCredentials inMemoryUserCredentials = userOptional.get();
        return inMemoryUserCredentials.getPassword().equals(userCredentials.getPassword());
    }


    //TODO - maybe those methods should return Optionals
    @Override
    public List<String> getUserConferences(String username) {
        return Arrays.asList(userConferences.get(username));
    }

    @Override
    public Optional<String> getConferenceUser(String conferenceName) {
        return userConferences.entrySet().stream()
                .filter(userConference -> userConference.getValue().equals(conferenceName))
                .map(Map.Entry::getKey)
                .findFirst();
    }

    @Override
    public boolean addUser(UserCredentials userCredentials) {
        return users.add(userCredentials);
    }

    @Override
    public boolean removeUser(String username) {
        Optional<UserCredentials> userCredentialsOptional =
                users.stream().filter(uc -> uc.getUsername().equals(username)).findAny();
        userCredentialsOptional.ifPresent(users::remove);

        return userCredentialsOptional.isPresent();
    }

    @Override
    public boolean addUserConference(String username, String conference) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean removeUserConference(String conference) {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean userExists(String username) {
        return users.stream().anyMatch(userCredentials -> userCredentials.getUsername().equals(username));
    }

    @Override
    public boolean conferenceExists(String conferenceName) {
        return userConferences.values().stream().anyMatch(cn -> cn.equals(conferenceName));
    }

    @Override
    public List<String> getUsers() {
        return users.stream().map(UserCredentials::getUsername).collect(Collectors.toList());
    }

    @Override
    public List<String> getConferences() {
        return Lists.newArrayList(userConferences.values());
    }

    private Set<UserCredentials> populateUsers() {
        Set<UserCredentials> users = new HashSet<>();
        users.add(new UserCredentials("user1", "password1"));
        users.add(new UserCredentials("user2", "password2"));
        users.add(new UserCredentials("user3", "password3"));

        return users;
    }

    private Map<String, String> populateUserConferences() {
        Map<String, String> userConferences = new HashMap<>();
        userConferences.put("user1", "conf1");
        userConferences.put("user2", "conf2");
        userConferences.put("user3", "conf3");

        return userConferences;
    }
}
