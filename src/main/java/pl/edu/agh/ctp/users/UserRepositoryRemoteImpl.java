package pl.edu.agh.ctp.users;

import com.google.common.base.Preconditions;
import pl.edu.agh.ctp.db.DatabaseConnector;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Created by Leszek Placzkiewicz on 2016-08-03.
 */
public class UserRepositoryRemoteImpl implements IUserRepository {

    private final DatabaseConnector databaseConnector;


    public UserRepositoryRemoteImpl(String username, String password, String serverName, int port) throws SQLException {
        Preconditions.checkNotNull(username);
        Preconditions.checkNotNull(password);
        Preconditions.checkNotNull(port);

        this.databaseConnector = new DatabaseConnector(username, password, serverName, port);
    }

    @Override
    public boolean authenticateUser(UserCredentials userCredentials) {
        String username = userCredentials.getUsername();
        String password = userCredentials.getPassword();

        String databasePassword = databaseConnector.getUserPassword(username);

        return password.equals(databasePassword);
    }

    @Override
    public List<String> getUserConferences(String username) {
        return databaseConnector.getUserConferences(username);
    }

    @Override
    public Optional<String> getConferenceUser(String conferenceName) {
        return databaseConnector.getConferenceUser(conferenceName);
    }

    @Override
    public boolean addUser(UserCredentials userCredentials) {
        String username = userCredentials.getUsername();
        String password = userCredentials.getPassword();
        return databaseConnector.addUser(username, password);
    }

    @Override
    public boolean removeUser(String username) {
        return databaseConnector.removeUser(username);
    }

    @Override
    public boolean addUserConference(String username, String conferenceName) {
        return databaseConnector.addUserConference(username, conferenceName);
    }

    @Override
    public boolean removeUserConference(String conferenceName) {
        return databaseConnector.removeConference(conferenceName);
    }

    @Override
    public boolean userExists(String username) {
        return databaseConnector.userExists(username);
    }

    @Override
    public boolean conferenceExists(String conferenceName) {
        return databaseConnector.conferenceExists(conferenceName);
    }

    @Override
    public List<String> getUsers() {
        return databaseConnector.getUsers();
    }

    @Override
    public List<String> getConferences() {
        return databaseConnector.getConferences();
    }
}