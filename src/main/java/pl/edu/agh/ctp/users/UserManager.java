package pl.edu.agh.ctp.users;

import com.google.common.base.Preconditions;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Leszek Placzkiewicz on 2016-08-03.
 */
public class UserManager {

    private final Scanner scanner;

    private final IUserRepository userRepository;

    private boolean shouldContinue;


    public UserManager(IUserRepository userRepository) {
        this.userRepository = Preconditions.checkNotNull(userRepository);
        this.scanner = new Scanner(System.in);
        this.shouldContinue = true;
    }

    public void run() {
        while (shouldContinue) {
            displayMenu();
            String userInput = readUserInput();
            parseUserInput(userInput);
        }
    }

    private void displayMenu() {
        String menu = "---------------------------------------------\n" +
                "au\t\tadd user\n" +
                "ru\t\tremove user\n" +
                "ac\t\tadd conference\n" +
                "rc\t\tremove conference\n" +
                "uc\t\tdisplay user conferences\n" +
                "lu\t\tlist users\n" +
                "ex\t\texit UserManager\n" +
                "---------------------------------------------\n";

        System.out.println(menu);
    }

    private void parseUserInput(String userInput) {
        switch (userInput.toLowerCase()) {
            case "au":
                addUser();
                break;
            case "ru":
                removeUser();
                break;
            case "ac":
                addConference();
                break;
            case "rc":
                removeConference();
                break;
            case "uc":
                userConferences();
                break;
            case "lu":
                listUsers();
                break;
            case "lc":
                listConferences();
                break;
            case "ex":
                shouldContinue = false;
                break;
            default:
                System.out.println("Invalid command. Try again.");
        }
    }

    private void addUser() {
        //TODO - validate username, e.g starts with letter not digit etc.
        System.out.print("Insert username: ");
        String username = readUserInput();

        //TODO - add masking input while inserting password
        System.out.print("Insert password: ");
        String password = readUserInput();

        System.out.print("Insert password again: ");
        String repeatedPassword = readUserInput();

        if (password.equals(repeatedPassword)) {
            boolean added = userRepository.addUser(new UserCredentials(username, password));
            if (added) {
                System.out.println("User has been succesfully added");
            } else {
                System.out.println("Error occurred while adding user");
            }
        } else {
            System.out.println("Repeated password doesn't match");
        }
    }

    private void removeUser() {
        System.out.print("Insert username: ");
        String username = readUserInput();

        if (userRepository.userExists(username)) {
            if (userRepository.getUserConferences(username).isEmpty()) {
                boolean removed = userRepository.removeUser(username);

                if (removed) {
                    System.out.println("Successfully removed user");
                } else {
                    System.out.println("Error occurred while removing user");
                }
            } else {
                System.out.println("This user manages some conferences. Remove conferences first.");
            }

        } else {
            System.out.println("No such user");
        }
    }

    private void addConference() {
        System.out.print("Insert conference name: ");
        String conferenceName = readUserInput();

        if (!userRepository.conferenceExists(conferenceName)) {
            System.out.print("Insert name of user that will manage the conference: ");
            String username = readUserInput();

            if (userRepository.userExists(username)) {
                boolean added = userRepository.addUserConference(username, conferenceName);
                if (added) {
                    System.out.println("Conference has been successfully added");
                } else {
                    System.out.println("Error occurred while adding conference");
                }
            } else {
                System.out.println("No such user");
            }
        } else {
            System.out.println("Conference already exists");
        }
    }

    private void removeConference() {
        System.out.print("Insert conference name: ");
        String conferenceName = readUserInput();

        if (userRepository.conferenceExists(conferenceName)) {
            boolean removed = userRepository.removeUserConference(conferenceName);
            if (removed) {
                System.out.println("Conference has been successfully removed");
            } else {
                System.out.println("Error occurred while removing conference");
            }
        } else {
            System.out.println("No such conference");
        }
    }

    private void userConferences() {
        System.out.print("Insert username: ");
        String username = readUserInput();

        if (userRepository.userExists(username)) {
            System.out.println(userRepository.getUserConferences(username));
        } else {
            System.out.println("No such user");
        }
    }

    private void listUsers() {
        List<String> users = userRepository.getUsers();
        System.out.println(users);
    }

    private void listConferences() {
        List<String> conferences = userRepository.getConferences();
        System.out.println(conferences);
    }

    private String readUserInput() {
        return scanner.nextLine();
    }

    public static void main(String[] args) throws SQLException {
        String username = System.getProperty("username");
        String password = System.getProperty("password");
        String serverName = System.getProperty("serverName");
        int port = Integer.valueOf(System.getProperty("port"));
        IUserRepository userRepository = new UserRepositoryRemoteImpl(username, password, serverName, port);
        UserManager userManager = new UserManager(userRepository);
        userManager.run();
    }
}