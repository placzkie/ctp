package pl.edu.agh.ctp.users;

import java.util.List;
import java.util.Optional;

/**
 * Created by Leszek Placzkiewicz on 2016-07-30.
 */
public interface IUserRepository {

    boolean authenticateUser(UserCredentials userCredentials);

    List<String> getUserConferences(String username);

    Optional<String> getConferenceUser(String conferenceName);

    boolean addUser(UserCredentials userCredentials);

    boolean removeUser(String username);

    boolean addUserConference(String username, String conference);

    boolean removeUserConference(String conference);

    boolean userExists(String username);

    boolean conferenceExists(String conferenceName);

    List<String> getUsers();

    List<String> getConferences();

}
