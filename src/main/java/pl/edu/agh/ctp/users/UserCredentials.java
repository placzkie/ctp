package pl.edu.agh.ctp.users;

import com.google.common.base.Preconditions;

/**
 * Created by Leszek Placzkiewicz on 2016-07-30.
 */
public class UserCredentials {

    private final String username;

    private final String password;


    public UserCredentials(String username, String password) {
        this.username = Preconditions.checkNotNull(username);
        this.password = Preconditions.checkNotNull(password);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
