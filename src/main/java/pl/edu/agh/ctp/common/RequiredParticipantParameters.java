package pl.edu.agh.ctp.common;

import com.google.common.base.Preconditions;
import pl.edu.agh.ctp.mcu.enums.ParticipantProtocol;
import pl.edu.agh.ctp.mcu.enums.ParticipantType;

/**
 * Created by Leszek Placzkiewicz on 2016-04-28.
 */
public class RequiredParticipantParameters {

    private final String conferenceName;

    private final String participantName;

    private final String autoAttendantUniqueID;

    private final ParticipantProtocol participantProtocol;

    private final ParticipantType participantType;


    private RequiredParticipantParameters(BuilderImpl builder) {
        Preconditions.checkNotNull(builder);

        this.conferenceName = builder.conferenceName;
        this.participantName = builder.participantName;
        this.autoAttendantUniqueID = builder.autoAttendantUniqueID;
        this.participantProtocol = builder.participantProtocol;
        this.participantType = builder.participantType;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public String getParticipantName() {
        return participantName;
    }

    public String getAutoAttendantUniqueID() {
        return autoAttendantUniqueID;
    }

    public ParticipantProtocol getParticipantProtocol() {
        return participantProtocol;
    }

    public ParticipantType getParticipantType() {
        return participantType;
    }

    @Override
    public String toString() {
        return "RequiredParticipantParameters{" +
                "conferenceName='" + conferenceName + '\'' +
                ", participantName='" + participantName + '\'' +
                ", autoAttendantUniqueID='" + autoAttendantUniqueID + '\'' +
                ", participantProtocol=" + participantProtocol +
                ", participantType=" + participantType +
                '}';
    }

    public static class BuilderImpl implements  ExpectingConferenceName,
                                                ExpectingParticipantName,
                                                ExpectingAutoAttendantUniqueID,
                                                ExpectingParticipantProtocol,
                                                ExpectingParticipantType,
                                                Builder {
        private String conferenceName;

        private String participantName;

        private String autoAttendantUniqueID;

        private ParticipantProtocol participantProtocol;

        private ParticipantType participantType;

        public static ExpectingConferenceName create() {
            return new BuilderImpl();
        }

        @Override
        public ExpectingParticipantName conferenceName(String conferenceName) {
            this.conferenceName = Preconditions.checkNotNull(conferenceName);
            return this;
        }

        @Override
        public ExpectingAutoAttendantUniqueID participantName(String participantName) {
            this.participantName = Preconditions.checkNotNull(participantName);
            return this;
        }

        @Override
        public ExpectingParticipantProtocol autoAttendantUniqueID(String autoAttendantUniqueID) {
            this.autoAttendantUniqueID = Preconditions.checkNotNull(autoAttendantUniqueID);
            return this;
        }

        @Override
        public ExpectingParticipantType participantProtocol(ParticipantProtocol participantProtocol) {
            this.participantProtocol = Preconditions.checkNotNull(participantProtocol);
            return this;
        }

        @Override
        public Builder participantType(ParticipantType participantType) {
            this.participantType = Preconditions.checkNotNull(participantType);
            return this;
        }

        @Override
        public RequiredParticipantParameters build() {
            return new RequiredParticipantParameters(this);
        }
    }

    public interface ExpectingConferenceName {
        ExpectingParticipantName conferenceName(String conferenceName);
    }

    public interface ExpectingParticipantName {
        ExpectingAutoAttendantUniqueID participantName(String participantName);
    }

    public interface ExpectingAutoAttendantUniqueID {
        ExpectingParticipantProtocol autoAttendantUniqueID(String autoAttendantUniqueID);
    }

    public interface ExpectingParticipantProtocol {
        ExpectingParticipantType participantProtocol(ParticipantProtocol participantProtocol);
    }

    public interface ExpectingParticipantType {
        Builder participantType(ParticipantType participantType);
    }

    public interface Builder {
        RequiredParticipantParameters build();
    }
}
