package pl.edu.agh.ctp.common;

import pl.edu.agh.ctp.console.InMemoryRepository;
import pl.edu.agh.ctp.mcu.McuResponseConstants;
import pl.edu.agh.ctp.mcu.enums.ParticipantProtocol;
import pl.edu.agh.ctp.mcu.enums.ParticipantType;

import java.util.Map;
import java.util.Optional;

/**
 * Created by Leszek Placzkiewicz on 2016-09-17.
 */
public class RequiredParticipantParametersHelper {

    private static final String NULL = "null";


    private final InMemoryRepository inMemoryRepository;

    public RequiredParticipantParametersHelper(InMemoryRepository inMemoryRepository) {
        this.inMemoryRepository = inMemoryRepository;
    }

    public Optional<RequiredParticipantParameters> obtainRequiredParticipantParameters(String participantName) {
        Optional<Map<String, Object>> participantOptional = inMemoryRepository.getParticipant(participantName);
        if (!participantOptional.isPresent()) {
            return Optional.empty();
        }

        Map<String, Object> participant = participantOptional.get();
        String conferenceName = (String) participant.getOrDefault(McuResponseConstants.CONFERENCE_NAME, NULL);
        String autoAttendantUniqueID =
                        (String) participant.getOrDefault(McuResponseConstants.AUTO_ATTENDANT_UNIQUE_ID, NULL);
        ParticipantProtocol participantProtocol =
                ParticipantProtocol.getByValue((String) participant.get(McuResponseConstants.PARTICIPANT_PROTOCOL));
        ParticipantType participantType =
                ParticipantType.getByValue((String) participant.get(McuResponseConstants.PARTICIPANT_TYPE));

        return Optional.of(RequiredParticipantParameters.BuilderImpl.create()
                .conferenceName(conferenceName)
                .participantName(participantName)
                .autoAttendantUniqueID(autoAttendantUniqueID)
                .participantProtocol(participantProtocol)
                .participantType(participantType)
                .build());
    }
}
