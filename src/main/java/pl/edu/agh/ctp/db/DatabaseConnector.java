package pl.edu.agh.ctp.db;

import com.google.common.base.Preconditions;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

/**
 * Created by Leszek Placzkiewicz on 2016-08-02.
 */
public class DatabaseConnector {

    private final String dbms = "mysql";

    private final String dbName = "ctp_supervisor";

    private final String userName;

    private final String password;

    private final String serverName;

    private final String portNumber;

    private Connection connection;


    public DatabaseConnector(String username, String password, String serverName, int port) throws SQLException {
        this.userName = Preconditions.checkNotNull(username);
        this.password = Preconditions.checkNotNull(password);
        this.serverName = Preconditions.checkNotNull(serverName);

        Preconditions.checkNotNull(port);
        this.portNumber = String.valueOf(port);
        this.connection = getConnection();
    }

    public boolean addUser(String userName, String password) {
        String query = "insert into " + dbName + ".USERS(Username, Password)" +
                "values('" + userName + "', '" + password + "')";
        return executeManipulationUpdate(query);
    }

    public boolean removeUser(String username) {
        String query = "delete from " + dbName + ".USERS where UserName = '" + username + "'";
        return executeManipulationUpdate(query);
    }

    public String getUserPassword(String userName) {
        String query =
                "select PASSWORD from " + dbName + ".USERS where USERNAME = '" + userName + "'";

        List<String> passwords = executeQuery(query, "Password");

        return passwords.stream().findFirst().orElse(null);
    }

    public List<String> getUserConferences(String username) {
        String query =
                "select ConferenceName from " + dbName + ".CONFERENCES join " + dbName + ".USERS on " +
                        "USERS.UserID = CONFERENCES.UserID where " +
                        "username = '" + username + "'";

        return executeQuery(query, "ConferenceName");
    }

    public Optional<String> getConferenceUser(String conferenceName) {
        String query = "select Username from " + dbName + ".CONFERENCES join " + dbName + ".USERS on " +
                "USERS.UserID = CONFERENCES.USERID where " +
                "conferenceName = '" + conferenceName + "'";

        List<String> username = executeQuery(query, "Username");
        return username.stream().findFirst();
    }

    public boolean addUserConference(String userName, String conferenceName) {
        int userId = getUserId(userName);
        String query = "insert into " + dbName + ".CONFERENCES(ConferenceName, UserID)" +
                " values('" + conferenceName + "','" + userId + "')";

        return executeManipulationUpdate(query);
    }

    public boolean removeConference(String conferenceName) {
        String query = "delete from " + dbName + ".CONFERENCES where ConferenceName = '" + conferenceName + "'";
        return executeManipulationUpdate(query);
    }

    public boolean userExists(String userName) {
        return getUserId(userName) != (-1);
    }

    public boolean conferenceExists(String conferenceName) {
        return getConferenceId(conferenceName) != -1;
    }

    public List<String> getUsers() {
        String query = "select Username from " + dbName + ".USERS";

        return executeQuery(query, "Username");
    }

    public List<String> getConferences() {
        String query = "select ConferenceName from " + dbName + ".CONFERENCES";

        return executeQuery(query, "ConferenceName");
    }

    private int getConferenceId(String conferenceName) {
        String query =
                "select ConferenceID from " + dbName + ".CONFERENCES where ConferenceName = '" + conferenceName + "'";

        List<String> conferenceIds = executeQuery(query, "ConferenceID");

        return conferenceIds.stream().findFirst().map(Integer::valueOf).orElse(-1);
    }

    private int getUserId(String userName) {
        String query = "select UserID from " + dbName + ".USERS where UserName = '" + userName + "'";

        List<String> userIds = executeQuery(query, "UserID");

        return userIds.stream().findFirst().map(Integer::valueOf).orElse(-1);
    }

    private List<String> executeQuery(String query, String column) {
        List<String> result = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                result.add(rs.getString(column));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    private boolean executeManipulationUpdate(String query) {
        int rowsAffected;
        try (Statement stmt = connection.createStatement()) {
            rowsAffected = stmt.executeUpdate(query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return rowsAffected > 0;
    }

    private Connection getConnection() throws SQLException {
        Properties connectionProperties = new Properties();
        connectionProperties.put("user", this.userName);
        connectionProperties.put("password", this.password);

        Connection connection = DriverManager.getConnection(
                "jdbc:" + this.dbms + "://" + this.serverName + ":" + this.portNumber + "/", connectionProperties);
        System.out.println("Connected to database");
        return connection;
    }
}
