package pl.edu.agh.ctp;

import org.apache.log4j.Logger;
import pl.edu.agh.ctp.authentication.UserValidator;
import pl.edu.agh.ctp.console.IMcuFacade;
import pl.edu.agh.ctp.console.InMemoryRepository;
import pl.edu.agh.ctp.console.McuFacadeLocalImpl;
import pl.edu.agh.ctp.feedback.ClientNotifier;
import pl.edu.agh.ctp.feedback.McuImitator;
import pl.edu.agh.ctp.feedback.RpcServer;
import pl.edu.agh.ctp.users.IUserRepository;
import pl.edu.agh.ctp.users.UserRepositoryLocalImpl;
import pl.edu.agh.ctp.websocket.RequestManager;
import pl.edu.agh.ctp.websocket.ResponseSender;
import pl.edu.agh.ctp.websocket.Server;
import pl.edu.agh.ctp.websocket.WebSocketRequestHandler;
import pl.edu.agh.ctp.websocket.queues.IncomingMessageQueue;
import pl.edu.agh.ctp.websocket.queues.OutgoingMessageQueue;

import java.net.UnknownHostException;

/**
 * Created by Leszek Placzkiewicz on 2016-07-10.
 */
public class Main {

    private static final Logger logger = Logger.getLogger(Main.class);

    private static final String REQUEST_MANAGER_THREAD_NAME = "RequestManagerThread";

    private static final String RESPONSE_SENDER_THREAD_NAME = "ResponseSenderThread";

    private static final String RPC_SERVER_THREAD_NAME = "RpcServerThread";

    private static final int PORT = 8887;//TODO - pass port while running server


    public static void main(String[] args) throws UnknownHostException {
        IncomingMessageQueue incomingMessageQueue = new IncomingMessageQueue();
        IUserRepository userRepository = new UserRepositoryLocalImpl();
        UserValidator userValidator = new UserValidator(userRepository);
        Server server = new Server(PORT, incomingMessageQueue, userValidator);
        server.start();
        logger.info("ChatServer started on port: " + server.getPort());

        McuImitator mcuImitator = new McuImitator();
        IMcuFacade mcuFacade = new McuFacadeLocalImpl(mcuImitator);
        OutgoingMessageQueue outgoingMessageQueue = new OutgoingMessageQueue();
        InMemoryRepository inMemoryRepository = new InMemoryRepository();
        spawnRequestManagerThread(mcuFacade,
                                    incomingMessageQueue,
                                    userRepository,
                                    outgoingMessageQueue,
                                    inMemoryRepository);
        logger.info("Request Manager thread spawned");

        spawnResponseSenderThread(outgoingMessageQueue, server);
        logger.info("Response Sender thread spawned");

        ClientNotifier clientNotifier =
                            new ClientNotifier(mcuFacade, userRepository, outgoingMessageQueue, inMemoryRepository);
        spawnRpcServerThread(clientNotifier);
        logger.info("RPC Server thread spawned");

        mcuImitator.registerClientNotifier(clientNotifier);
    }

    private static void spawnRequestManagerThread(IMcuFacade mcuFacade,
                                                  IncomingMessageQueue incomingMessageQueue,
                                                  IUserRepository userRepository,
                                                  OutgoingMessageQueue outgoingMessageQueue,
                                                  InMemoryRepository inMemoryRepository) {
        WebSocketRequestHandler webSocketRequestHandler =
                new WebSocketRequestHandler(mcuFacade, inMemoryRepository, userRepository);
        RequestManager requestManager =
                new RequestManager(incomingMessageQueue, webSocketRequestHandler, outgoingMessageQueue);
        Thread requestManagerThread = new Thread(requestManager, REQUEST_MANAGER_THREAD_NAME);
        requestManagerThread.start();
    }

    private static void spawnResponseSenderThread(OutgoingMessageQueue outgoingMessageQueue, Server server) {
        ResponseSender responseSender = new ResponseSender(outgoingMessageQueue, server);
        Thread responseSenderThread = new Thread(responseSender, RESPONSE_SENDER_THREAD_NAME);
        responseSenderThread.start();
    }

    private static void spawnRpcServerThread(ClientNotifier clientNotifier) {
        RpcServer rpcServer = new RpcServer(clientNotifier);
        Thread rpcServerThread = new Thread(rpcServer, RPC_SERVER_THREAD_NAME);
        rpcServerThread.start();
    }
}
