package pl.edu.agh.ctp;

import com.google.common.base.Preconditions;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Leszek Placzkiewicz on 2016-09-21.
 */
public class Conference {

    @SerializedName("conferenceName")
    private final String conferenceName;

    @SerializedName("focusType")
    private final String focusType;

    @SerializedName("focusParticipantName")
    private final String focusParticipantName;

    @SerializedName("currentLayout")
    private final int currentLayout;

    @SerializedName("layoutSource")
    private final String layoutSource;//one of familyx, conferenceCustom, participantCustom? Is participantCustom
                                        //possible in here???

    //TODO - builder
    public Conference(String conferenceName,
                      String focusType,
                      String focusParticipantName,
                      int currentLayout,
                      String layoutSource) {
        this.conferenceName = Preconditions.checkNotNull(conferenceName);
        this.focusType = Preconditions.checkNotNull(focusType);
        this.focusParticipantName = focusParticipantName;
        this.currentLayout = currentLayout;
        this.layoutSource = layoutSource;
    }

    public String getConferenceName() {
        return conferenceName;
    }

    public String getFocusType() {
        return focusType;
    }

    public String getFocusParticipantName() {
        return focusParticipantName;
    }

    public int getCurrentLayout() {
        return currentLayout;
    }

    public String getLayoutSource() {
        return layoutSource;
    }
}
