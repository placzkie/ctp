package pl.edu.agh.ctp.feedback;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Leszek Placzkiewicz on 2016-06-08.
 */
public class RpcServer implements Runnable {

    private static final Logger logger = Logger.getLogger(RpcServer.class);


    private final ClientNotifier clientNotifier;


    public RpcServer(ClientNotifier clientNotifier) {
        this.clientNotifier = clientNotifier;
    }

    @Override
    public void run() {
        HttpServer server = null;
        try {
            server = HttpServer.create(new InetSocketAddress(8080), 0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        server.createContext("/RPC2", new MyHandler(clientNotifier));
        server.setExecutor(null);
        server.start();
        logger.info("RpcServer started");
    }

    public static class MyHandler implements HttpHandler {

        private final ClientNotifier clientNotifier;

        public MyHandler(ClientNotifier clientNotifier) {
            this.clientNotifier = clientNotifier;
        }

        @Override
        public void handle(HttpExchange t) throws IOException {
            StringBuilder stringBuilder = new StringBuilder();
            InputStream requestBody = t.getRequestBody();
            Scanner scanner = new Scanner(requestBody);
            while(scanner.hasNext()) {
                stringBuilder.append(scanner.nextLine());
            }

            t.sendResponseHeaders(200, 0);
            t.close();

            Set<String> events = extractEvents(stringBuilder.toString());
            logger.debug("Received feedback: " + events);

            clientNotifier.updateClient(events);
        }

        private Set<String> extractEvents(String xml) {
            int eventsIndex = xml.indexOf("events");
            int stringBeginIndex = xml.indexOf("<string>", eventsIndex);
            int stringEndIndex = xml.indexOf("</string>", stringBeginIndex);
            String eventsString = xml.substring(stringBeginIndex + 8, stringEndIndex);
            List<String> events = Arrays.asList(eventsString.split(","));
            return events.stream().map(String::trim).collect(Collectors.toSet());
        }
    }
}