package pl.edu.agh.ctp.feedback;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import pl.edu.agh.ctp.Conference;
import pl.edu.agh.ctp.Participant;
import pl.edu.agh.ctp.console.IMcuFacade;
import pl.edu.agh.ctp.console.InMemoryRepository;
import pl.edu.agh.ctp.users.IUserRepository;
import pl.edu.agh.ctp.websocket.enums.OutgoingMessageType;
import pl.edu.agh.ctp.websocket.messages.OutgoingMessage;
import pl.edu.agh.ctp.websocket.queues.OutgoingMessageQueue;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static pl.edu.agh.ctp.websocket.enums.OutgoingMessageType.CONFERENCE_STATE;
import static pl.edu.agh.ctp.websocket.enums.OutgoingMessageType.GET_CONFERENCE_CONFIG;
import static pl.edu.agh.ctp.websocket.enums.OutgoingMessageType.GET_PARTICIPANTS;

/**
 * Created by Leszek Placzkiewicz on 2016-08-07.
 */
public class ClientNotifier {

    private final Set<String> allowableEvents;

    private final IMcuFacade mcuFacade;

    private final IUserRepository userRepository;

    private final OutgoingMessageQueue outgoingMessageQueue;

    private final InMemoryRepository inMemoryRepository;

    public ClientNotifier(IMcuFacade mcuFacade,
                          IUserRepository userRepository,
                          OutgoingMessageQueue outgoingMessageQueue,
                          InMemoryRepository inMemoryRepository) {
        this.allowableEvents = prepareAllowableEvents();
        this.mcuFacade = mcuFacade;
        this.userRepository = userRepository;
        this.outgoingMessageQueue = outgoingMessageQueue;
        this.inMemoryRepository = inMemoryRepository;
    }

    public void updateClient(Set<String> events) {
        events.retainAll(allowableEvents);
        events.stream().forEach(this::handleEvent);
    }

    private void handleEvent(String event) {
        switch (event) {
            case "participantJoined":
                updateParticipants();
                break;
            case "participantLeft":
                updateParticipants();
                break;
            case "conferenceStarted":
                updateConferencesStates();
                break;
            case "conferenceFinished":
                updateConferencesStates();
                break;
            case "participantConnected":
                //TODO - Do I need to handle it
                break;
            case "participantDisconnected":
                //TODO - Do I need to handle it
                break;
            case "conferenceConfigurationChanged":
                updateConferencesConfigs();
                break;
            case "importanceChanged":
                updateParticipants();
            default:
                System.out.println("Unhandled event: " + event);
        }

    }

    private void updateParticipants() {
        List<Map<String, Object>> participants = mcuFacade.enumerateParticipants();
        Map<String, List<Participant>> conferenceParticipants = participants.stream()
                .collect(Collectors.groupingBy(participant -> (String) participant.get("conferenceName"),
                        Collectors.mapping(x -> new Participant(String.valueOf(x.get("participantName")),
                                Boolean.valueOf(
                                        ((Map<String, Object>) x.get("currentState")).get("audioRxMuted").toString()),
                                ((Map<String, Object>) x.get("currentState")).get("displayName").toString(),
                                Boolean.valueOf(
                                        ((Map<String, Object>) x.get("currentState")).get("important").toString()),
                                Boolean.valueOf(
                                        ((Map<String, Object>) x.get("currentState")).get("activeSpeaker").toString()),
                                Boolean.valueOf(
                                        ((Map<String, Object>) x.get("currentState")).get("lecturer").toString())
                        ), Collectors.toList())));

        for (Map.Entry<String, List<Participant>> conPart : conferenceParticipants.entrySet()) {
            Optional<String> conferenceUser = userRepository.getConferenceUser(conPart.getKey());

            Map<String, Object> additionalProperties = Maps.newHashMap();
            additionalProperties.put("participants", conPart.getValue());
            conferenceUser.ifPresent(user -> sendNotification(user, GET_PARTICIPANTS, additionalProperties));
        }
    }

    private void updateConferencesStates() {
        //TODO - currently enumerate returns only active conferences - make sure it is proper attitude
        List<Map<String, Object>> conferences = mcuFacade.enumerateConferences();
        Map<String, Boolean> conferenceStates = conferences.stream()
                .collect(Collectors.toMap(conference -> (String) conference.get("conferenceName"),
                        conference -> (Boolean) conference.get("conferenceActive")));

        for (Map.Entry<String, Boolean> conStat : conferenceStates.entrySet()) {
            Optional<String> conferenceUser = userRepository.getConferenceUser(conStat.getKey());

            Map<String, Object> additionalProperties = Maps.newHashMap();
            additionalProperties.put("conferenceActive", conStat.getValue());
            conferenceUser.ifPresent(user -> sendNotification(user, CONFERENCE_STATE, additionalProperties));
        }
    }

    private void updateConferencesConfigs() {
        List<String> conferenceNames = mcuFacade.enumerateConferences()
                                                .stream()
                                                .map(conf -> (String) conf.get("conferenceName"))
                                                .collect(Collectors.toList());

        List<Conference> conferences = Lists.newArrayList();
        for (String conferenceName : conferenceNames) {
            Map<String, Object> conference = mcuFacade.queryConferenceStreaming(conferenceName);
            String focusType = (String) conference.get("focusType");

            Object focusParticipant = conference.getOrDefault("focusParticipant", null);
            String focusParticipantName = null;
            if (focusParticipant != null) {
                //TODO - so ugly
                focusParticipantName = (String) ((Map<String, Object>) focusParticipant).get("participantName");
            }

            int currentLayout = (int) conference.get("currentLayout");
            String layoutSource = (String) conference.get("layoutSource");

            conferences.add(
                    new Conference(conferenceName, focusType, focusParticipantName, currentLayout, layoutSource));
        }

        for (Conference conference : conferences) {
            Optional<String> conferenceUser = userRepository.getConferenceUser(conference.getConferenceName());

            Map<String, Object> properties = Maps.newHashMap();
            properties.put("conference", conference);

            conferenceUser.ifPresent(user -> sendNotification(user, GET_CONFERENCE_CONFIG, properties));
        }
    }

    private void sendNotification(String username,
                                  OutgoingMessageType outgoingMessageType, Map<String, Object> additionalProperties) {
        OutgoingMessage outgoingMessage = new OutgoingMessage(outgoingMessageType, username, additionalProperties);
        outgoingMessageQueue.offer(outgoingMessage);
    }

    private Set<String> prepareAllowableEvents() {
        //TODO - complement, replenish
        return Sets.newHashSet("participantJoined",
                "participantLeft",
                "conferenceStarted",
                "conferenceFinished",
                "participantConnected",
                "participantDisconnected",
                "conferenceConfigurationChanged",
                "importanceChanged");
    }
}
