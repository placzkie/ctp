package pl.edu.agh.ctp.feedback;

import java.util.Set;

/**
 * Created by Leszek Placzkiewicz on 2016-09-24.
 */
public class McuImitator {

    public static final McuImitator DEFAULT_IMITATOR = new McuImitator() {
        @Override
        public void updateClient(Set<String> events) {}
    };

    private ClientNotifier clientNotifier;

    public void updateClient(Set<String> events) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        clientNotifier.updateClient(events);
    }

    public void registerClientNotifier(ClientNotifier clientNotifier) {
        this.clientNotifier = clientNotifier;
    }

}
