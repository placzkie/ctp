package pl.edu.agh.ctp.mcu.enums;

/**
 * Created by Leszek Placzkiewicz on 01.04.16.
 */
public enum McuMethodParam {
    AUTHENTICATION_USER("authenticationUser"),
    AUTHENTICATION_PASSWORD("authenticationPassword"),
    CONFERENCE_NAME("conferenceName"),
    CONFERENCE_PIN("pin"),
    PARTICIPANT_NAME("participantName"),
    PARTICIPANT_TYPE("participantType"),
    OPERATION_SCOPE("operationScope"),
    ENUMERATE_ID("enumerateID"),
    AUTO_ATTENDANT_UNIQUE_ID("autoAttendantUniqueID"),
    PARTICIPANT_PROTOCOL("participantProtocol"),
    DIRECTION("direction"),
    MESSAGE("message"),
    RECEIVER_URI("receiverURI"),
    RECEIVER_INDEX("receiverIndex"),
    ADDRESS("address");

    private final String paramName;


    McuMethodParam(String paramName) {
        this.paramName = paramName;
    }

    @Override
    public String toString() {
        return paramName;
    }
}
