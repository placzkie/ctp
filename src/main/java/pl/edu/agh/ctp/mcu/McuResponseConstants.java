package pl.edu.agh.ctp.mcu;

/**
 * Created by Leszek Placzkiewicz on 2016-09-17.
 */
public class McuResponseConstants {

    public static final String CONFERENCE_NAME = "conferenceName";

    public static final String AUTO_ATTENDANT_UNIQUE_ID = "autoAttendantUniqueID";

    public static final String PARTICIPANT_PROTOCOL = "participantProtocol";

    public static final String PARTICIPANT_TYPE = "participantType";

    public static final String PARTICIPANT_NAME = "participantName";
}
