package pl.edu.agh.ctp.mcu.enums;

/**
 * Created by Leszek Placzkiewicz on 07.04.16.
 */
public enum OperationScope {
    CONFIGURED_STATE("configuredState"),
    CURRENT_STATE("currentState"),
    ACTIVE_STATE("activeState");

    private final String value;


    OperationScope(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
