package pl.edu.agh.ctp.mcu.enums;

/**
 * Created by Leszek Placzkiewicz on 07.04.16.
 */
public enum Direction {
    UP("up"),
    DOWN("down"),
    LEFT("left"),
    RIGHT("right"),
    ZOOM_IN("zoomIn"),
    ZOOM_OUT("zoomOut"),
    FOCUS_IN("focusIn"),
    FOCUS_OUT("focusOut");

    private final String value;


    Direction(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
