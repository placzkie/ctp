package pl.edu.agh.ctp.mcu.enums;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by Leszek Placzkiewicz on 07.04.16.
 */
public enum ParticipantType {
    BY_ADDRESS("by_address"),
    BY_NAME("by_name"),
    AD_HOC("ad_hoc");

    private final String value;


    ParticipantType(String value) {
        this.value = value;
    }

    public static ParticipantType getByValue(String value) {
        ParticipantType[] values = ParticipantType.values();
        Optional<ParticipantType> optional = Arrays.stream(values).filter(e -> e.value.equals(value)).findAny();
        if (!optional.isPresent()) {
            throw new EnumConstantNotPresentException(ParticipantProtocol.class, value);
        }
        return optional.get();
    }

    @Override
    public String toString() {
        return value;
    }
}
