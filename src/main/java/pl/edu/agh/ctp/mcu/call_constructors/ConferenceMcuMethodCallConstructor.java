package pl.edu.agh.ctp.mcu.call_constructors;

import com.google.common.base.Preconditions;
import pl.edu.agh.ctp.mcu.enums.McuMethodName;
import pl.edu.agh.ctp.mcu.enums.McuMethodParam;
import pl.edu.agh.ctp.mcu.enums.McuMethodReturnedParam;
import pl.edu.agh.ctp.mcu.sender.RequestSender;
import pl.edu.agh.ctp.mcu.utils.McuMethodCallUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Leszek Placzkiewicz on 01.04.16.
 */
public class ConferenceMcuMethodCallConstructor {

    private final String authenticationUser;

    private final String authenticationPassword;

    private final RequestSender requestSender;


    public ConferenceMcuMethodCallConstructor(String authenticationUser,
                                              String authenticationPassword,
                                              RequestSender requestSender) {
        this.authenticationUser = Preconditions.checkNotNull(authenticationUser);
        this.authenticationPassword = Preconditions.checkNotNull(authenticationPassword);
        this.requestSender = Preconditions.checkNotNull(requestSender);
    }

    /*public Map<String, Object> createConference(String conferenceName, String pin) {
        Preconditions.checkNotNull(conferenceName);
        Preconditions.checkNotNull(pin);

        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.CONFERENCE_NAME, conferenceName);
        params.put(McuMethodParam.CONFERENCE_PIN, pin);

        return requestSender.sendRequest(McuMethodName.CONFERENCE_CREATE,
                McuMethodCallUtils.prepareMapToSend(params));
    }*/

    /*public Map<String, Object> destroyConference(String conferenceName) {
        Preconditions.checkNotNull(conferenceName);

        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.CONFERENCE_NAME, conferenceName);

        return requestSender.sendRequest(McuMethodName.CONFERENCE_DESTROY,
                McuMethodCallUtils.prepareMapToSend(params));
    }*/

    /*
        public Map<String, Object> endConference(String conferenceName) {
            Preconditions.checkNotNull(conferenceName);

            Map<McuMethodParam, Object> params =
                    McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
            params.put(McuMethodParam.CONFERENCE_NAME, conferenceName);

            return requestSender.sendRequest(McuMethodName.CONFERENCE_END,
                    McuMethodCallUtils.prepareMapToSend(params));
        }

    */
    public List<Object> enumerateConferences() {
        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);

        List<Object> conferenceList = new ArrayList<>();
        Map<String, Object> result;
        do {
            Map<String, Object> paramsMap = McuMethodCallUtils.prepareMapToSend(params);
            paramsMap.put("enumerateFilter", "active");
            result = requestSender.sendRequest(McuMethodName.CONFERENCE_ENUMERATE, paramsMap);
            addConferencesToList(conferenceList, result);

            if (shouldContinueEnumeration(result)) {
                String enumerateID = (String) result.get(McuMethodParam.ENUMERATE_ID.toString());
                params.put(McuMethodParam.ENUMERATE_ID, enumerateID);
            }
        } while (shouldContinueEnumeration(result));

        return conferenceList;
    }

    private void addConferencesToList(List<Object> conferenceList, Map<String, Object> result) {
        Object[] conferences = (Object[]) result.get(McuMethodReturnedParam.CONFERENCES.toString());
        conferenceList.addAll(Arrays.asList(conferences));
    }

    private boolean shouldContinueEnumeration(Map<String, Object> map) {
        return map.containsKey(McuMethodParam.ENUMERATE_ID.toString());
    }

    public void modifyConference(String conferenceName, Map<String, Object> optionalParameters) {
        Preconditions.checkNotNull(conferenceName);
        Preconditions.checkNotNull(optionalParameters);

        Map<McuMethodParam, Object> params =
                                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.CONFERENCE_NAME, conferenceName);

        Map<String, Object> paramsMap = McuMethodCallUtils.prepareMapToSend(params);
        paramsMap.putAll(optionalParameters);

        requestSender.sendRequest(McuMethodName.CONFERENCE_MODIFY, paramsMap);
    }

    public void modifyConferenceStreaming(String conferenceName, Map<String, Object> optionalParameters) {
        Preconditions.checkNotNull(conferenceName);
        Preconditions.checkNotNull(optionalParameters);

        Map<McuMethodParam, Object> params =
                                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.CONFERENCE_NAME, conferenceName);

        Map<String, Object> paramsMap = McuMethodCallUtils.prepareMapToSend(params);
        paramsMap.putAll(optionalParameters);

        requestSender.sendRequest(McuMethodName.CONFERENCE_STREAMING_MODIFY, paramsMap);
    }

    //TODO - test in practice
    public Map<String, Object> queryConferenceStreaming(String conferenceName) {
        Preconditions.checkNotNull(conferenceName);

        Map<McuMethodParam, Object> params =
                                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.CONFERENCE_NAME, conferenceName);

        Map<String, Object> paramsMap = McuMethodCallUtils.prepareMapToSend(params);

        return requestSender.sendRequest(McuMethodName.CONFERENCE_STREAMING_QUERY, paramsMap);
    }
}
