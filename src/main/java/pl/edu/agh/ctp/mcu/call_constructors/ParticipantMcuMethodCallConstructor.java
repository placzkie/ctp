package pl.edu.agh.ctp.mcu.call_constructors;

import com.google.common.base.Preconditions;
import pl.edu.agh.ctp.common.RequiredParticipantParameters;
import pl.edu.agh.ctp.mcu.enums.*;
import pl.edu.agh.ctp.mcu.sender.RequestSender;
import pl.edu.agh.ctp.mcu.utils.McuMethodCallUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Leszek Placzkiewicz on 07.04.16.
 */
public class ParticipantMcuMethodCallConstructor {

    private final String authenticationUser;

    private final String authenticationPassword;

    private final RequestSender requestSender;


    public ParticipantMcuMethodCallConstructor(String authenticationUser,
                                               String authenticationPassword,
                                               RequestSender requestSender) {
        this.authenticationUser = Preconditions.checkNotNull(authenticationUser);
        this.authenticationPassword = Preconditions.checkNotNull(authenticationPassword);
        this.requestSender = Preconditions.checkNotNull(requestSender);
    }

    //TODO it can get autoAttendantUniqueId instead of conferenceName
    public Map<String, Object> statusParticipant(RequiredParticipantParameters requiredParticipantParameters) {
        Preconditions.checkNotNull(requiredParticipantParameters);

        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.CONFERENCE_NAME, requiredParticipantParameters.getConferenceName());
        params.put(McuMethodParam.PARTICIPANT_NAME, requiredParticipantParameters.getParticipantName());
//        params.put(McuMethodParam.AUTO_ATTENDANT_UNIQUE_ID, requiredParticipantParameters.getAutoAttendantUniqueID());
        params.put(McuMethodParam.PARTICIPANT_PROTOCOL,
                                                requiredParticipantParameters.getParticipantProtocol().toString());
        params.put(McuMethodParam.PARTICIPANT_TYPE, requiredParticipantParameters.getParticipantType().toString());
        params.put(McuMethodParam.OPERATION_SCOPE,
                                        new Object[]{OperationScope.CURRENT_STATE, OperationScope.CONFIGURED_STATE});

        return requestSender.sendRequest(McuMethodName.PARTICIPANT_STATUS, McuMethodCallUtils.prepareMapToSend(params));
    }


    //parameter layoutControlEx - defines how the view layout can be controlled
    public void modifyParticipant(RequiredParticipantParameters requiredParticipantParameters,
                                  OperationScope operationScope,
                                  Map<String, Object> optionalParams) {//TODO - it allows any parameter now- use enum to restrict
        Preconditions.checkNotNull(requiredParticipantParameters);
        Preconditions.checkNotNull(optionalParams);

        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.CONFERENCE_NAME, requiredParticipantParameters.getConferenceName());
        params.put(McuMethodParam.PARTICIPANT_NAME, requiredParticipantParameters.getParticipantName());
//        params.put(McuMethodParam.AUTO_ATTENDANT_UNIQUE_ID, requiredParticipantParameters.getAutoAttendantUniqueID());
        params.put(McuMethodParam.PARTICIPANT_PROTOCOL,
                                                requiredParticipantParameters.getParticipantProtocol().toString());
        params.put(McuMethodParam.PARTICIPANT_TYPE, requiredParticipantParameters.getParticipantType().toString());
        params.put(McuMethodParam.OPERATION_SCOPE, operationScope.toString());

        Map<String, Object> convertedParams = McuMethodCallUtils.prepareMapToSend(params);
        convertedParams.putAll(optionalParams);

        requestSender.sendRequest(McuMethodName.PARTICIPANT_MODIFY, convertedParams);
    }

    //TODO - conferenceName or autoAttendantUniqueID - can both be sent?
    //No only one parameter can be passed - TODO - implement null checkinig and appropriate parameter passing
    public void feccParticipant(RequiredParticipantParameters requiredParticipantParameters,
                                Direction direction) {
        Preconditions.checkNotNull(requiredParticipantParameters);
        Preconditions.checkNotNull(direction);

        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.CONFERENCE_NAME, requiredParticipantParameters.getConferenceName());
        params.put(McuMethodParam.PARTICIPANT_NAME, requiredParticipantParameters.getParticipantName());
//        params.put(McuMethodParam.AUTO_ATTENDANT_UNIQUE_ID, requiredParticipantParameters.getAutoAttendantUniqueID());
        params.put(McuMethodParam.PARTICIPANT_PROTOCOL,
                                            requiredParticipantParameters.getParticipantProtocol().toString());
        params.put(McuMethodParam.PARTICIPANT_TYPE, requiredParticipantParameters.getParticipantType().toString());
        params.put(McuMethodParam.DIRECTION, direction.toString());

        requestSender.sendRequest(McuMethodName.PARTICIPANT_FECC, McuMethodCallUtils.prepareMapToSend(params));
    }

    //TODO conferenceName or autoAttendantUniqueID
    public void messageParticipant(RequiredParticipantParameters requiredParticipantParameters,
                                   String message) {
        Preconditions.checkNotNull(requiredParticipantParameters);
        Preconditions.checkNotNull(message);

        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.CONFERENCE_NAME, requiredParticipantParameters.getConferenceName());
        params.put(McuMethodParam.PARTICIPANT_NAME, requiredParticipantParameters.getParticipantName());
//        params.put(McuMethodParam.AUTO_ATTENDANT_UNIQUE_ID, requiredParticipantParameters.getAutoAttendantUniqueID());
        params.put(McuMethodParam.PARTICIPANT_PROTOCOL,
                                            requiredParticipantParameters.getParticipantProtocol().toString());
        params.put(McuMethodParam.PARTICIPANT_TYPE, requiredParticipantParameters.getParticipantType().toString());
        params.put(McuMethodParam.MESSAGE, message);

        requestSender.sendRequest(McuMethodName.PARTICIPANT_MESSAGE, McuMethodCallUtils.prepareMapToSend(params));
    }

    /*public Map<String, Object> addParticipant(String participantName, String conferenceName) {
        Preconditions.checkNotNull(participantName);
        Preconditions.checkNotNull(conferenceName);

        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.PARTICIPANT_NAME, participantName);
        params.put(McuMethodParam.CONFERENCE_NAME, conferenceName);
        params.put(McuMethodParam.PARTICIPANT_TYPE, ParticipantType.BY_ADDRESS);

        return requestSender.sendRequest(McuMethodName.PARTICIPANT_ADD,
                McuMethodCallUtils.prepareMapToSend(params));
    }*/

    public void addParticipant(String conferenceName, String participantName, String address) {
        Preconditions.checkNotNull(conferenceName);
        Preconditions.checkNotNull(participantName);
        Preconditions.checkNotNull(address);

        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.CONFERENCE_NAME, conferenceName);
        params.put(McuMethodParam.PARTICIPANT_NAME, participantName);
        params.put(McuMethodParam.ADDRESS, address);
        params.put(McuMethodParam.PARTICIPANT_TYPE, ParticipantType.BY_ADDRESS);

        requestSender.sendRequest(McuMethodName.PARTICIPANT_ADD, McuMethodCallUtils.prepareMapToSend(params));
    }

    public List<Object> enumerateParticipants() {
        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.OPERATION_SCOPE, new Object[]{
                OperationScope.CURRENT_STATE.toString() /*, OperationScope.CONFIGURED_STATE*/});

        List<Object> participantsList = new ArrayList<>();
        Map<String, Object> result;
        do {

//            Map<String, Object> paramsMap = McuMethodCallUtils.prepareMapToSend(params);
//            paramsMap.put("disconnected", "true");
            result = requestSender.sendRequest(McuMethodName.PARTICIPANT_ENUMERATE,
                                                                        McuMethodCallUtils.prepareMapToSend(params));

            addParticipantsToList(participantsList, result);

            if (shouldContinueEnumeration(result)) {
                String enumerateID = (String) result.get(McuMethodParam.ENUMERATE_ID.toString());
                params.put(McuMethodParam.ENUMERATE_ID, enumerateID);
            }
        } while (shouldContinueEnumeration(result));

        return participantsList;
    }

    private void addParticipantsToList(List<Object> participantsList, Map<String, Object> result) {
        Object[] participants = (Object[]) result.get(McuMethodReturnedParam.PARTICIPANTS.toString());
        participantsList.addAll(Arrays.asList(participants));
    }

    private boolean shouldContinueEnumeration(Map<String, Object> map) {
        return map.containsKey(McuMethodParam.ENUMERATE_ID.toString());
    }
}
