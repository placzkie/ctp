package pl.edu.agh.ctp.mcu.call_constructors;

import com.google.common.base.Preconditions;
import pl.edu.agh.ctp.mcu.enums.McuMethodName;
import pl.edu.agh.ctp.mcu.enums.McuMethodParam;
import pl.edu.agh.ctp.mcu.sender.RequestSender;
import pl.edu.agh.ctp.mcu.utils.McuMethodCallUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Leszek Placzkiewicz on 2016-06-02.
 */
public class FeedbackMcuMethodCallConstructor {

    private final String authenticationUser;

    private final String authenticationPassword;

    private final RequestSender requestSender;


    public FeedbackMcuMethodCallConstructor(String authenticationUser,
                                            String authenticationPassword,
                                            RequestSender requestSender) {
        this.authenticationUser = Preconditions.checkNotNull(authenticationUser);
        this.authenticationPassword = Preconditions.checkNotNull(authenticationPassword);
        this.requestSender = Preconditions.checkNotNull(requestSender);
    }

    public void registerFeedback(String address) {
        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        params.put(McuMethodParam.RECEIVER_URI, address);
        params.put(McuMethodParam.RECEIVER_INDEX, -1);
        requestSender.sendRequest(McuMethodName.FEEDBACK_CONFIGURE, McuMethodCallUtils.prepareMapToSend(params));
    }

    public List<Object> queryFeedback() {
        Map<McuMethodParam, Object> params =
                McuMethodCallUtils.createDefaultMap(authenticationUser, authenticationPassword);
        Map<String, Object> result =
                requestSender.sendRequest(McuMethodName.FEEDBACK_QUERY, McuMethodCallUtils.prepareMapToSend(params));
        Object[] receivers = (Object[]) result.get("receivers");
        return Arrays.asList(receivers);
    }
}
