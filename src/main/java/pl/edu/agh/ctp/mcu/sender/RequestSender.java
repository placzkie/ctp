package pl.edu.agh.ctp.mcu.sender;

import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import pl.edu.agh.ctp.mcu.enums.McuMethodName;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Created by Leszek Placzkiewicz on 2016-03-31.
 */
public class RequestSender {

    private static final Logger logger = Logger.getLogger(RequestSender.class);

    private final String CONFIG_FILE_PATH = "./src/main/resources/mcu.config";

    private final XmlRpcClient client;

    private static RequestSender instance = null;


    public static RequestSender getInstance() {
        if( instance == null) {
            instance = new RequestSender();
        }
        return instance;
    }

    private RequestSender() {
        client = createClient();
    }

    public Map<String, Object> sendRequest(McuMethodName mcuMethodName, Map<String, Object> params) {
        logger.debug("Sending request to MCU: [mcuMethodName=" + mcuMethodName + ", params=" + params + "]");

        try {
            String method = mcuMethodName.toString();
            Map<String, Object> result = (Map<String, Object>) client.execute(method, new Object[]{params});
            return result;
        } catch (XmlRpcException e) {
            throw new RuntimeException(e);
        }
    }

    private XmlRpcClient createClient() {
        XmlRpcClientConfigImpl config = prepareConfig();
        XmlRpcClient client = new XmlRpcClient();
        client.setConfig(config);
//        client.setTypeFactory(new MyStringFactory(new XmlRpcClient()));
        return client;
    }

    private XmlRpcClientConfigImpl prepareConfig() {
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        String address = readMcuConfig(CONFIG_FILE_PATH);
        config.setServerURL(convertToUrl(address));
        config.setEncoding(XmlRpcClientConfigImpl.UTF8_ENCODING);
        return config;
    }

    private String readMcuConfig(String filePath) {
        Path path = Paths.get(filePath);
        try {
            //TODO maybe check if it is provided with property first, for example: mcuip=172.24.65.50/RPC2
            return Files.lines(path).findFirst().get();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private URL convertToUrl(String address) {
        try {
            return new URL(address);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
