package pl.edu.agh.ctp.mcu.sender;

import org.apache.xmlrpc.common.TypeFactoryImpl;
import org.apache.xmlrpc.common.XmlRpcController;
import org.apache.xmlrpc.common.XmlRpcStreamConfig;
import org.apache.xmlrpc.serializer.TypeSerializer;
import org.apache.xmlrpc.serializer.TypeSerializerImpl;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * Created by Leszek Placzkiewicz on 31.03.16.
 */
public class MyStringFactory extends TypeFactoryImpl {

    public MyStringFactory(XmlRpcController pController) {
        super(pController);
    }

    public TypeSerializer getSerializer(XmlRpcStreamConfig config, Object object) throws SAXException {
        if (object instanceof String) {
            return new MyStringSerializer();
        } else {
            return super.getSerializer(config, object);
        }
    }

    private class MyStringSerializer extends TypeSerializerImpl {
        /** (Optional) Tag name of a string value.
         */
        public static final String STRING_TAG = "string";

        public void write(ContentHandler handler, Object object) throws SAXException {
            write(handler, STRING_TAG, object.toString());
        }
    }
}