package pl.edu.agh.ctp.mcu.enums;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by Leszek Placzkiewicz on 07.04.16.
 */
public enum ParticipantProtocol {
    H_323("h323"),
    SIP("sip"),
    VNC("vnc");

    private final String value;


    ParticipantProtocol(String value) {
        this.value = value;
    }

    public static ParticipantProtocol getByValue(String value) {
        ParticipantProtocol[] values = ParticipantProtocol.values();
        Optional<ParticipantProtocol> optional = Arrays.stream(values).filter(e -> e.value.equals(value)).findAny();
        if (!optional.isPresent()) {
            throw new EnumConstantNotPresentException(ParticipantProtocol.class, value);
        }
        return optional.get();
    }

    @Override
    public String toString() {
        return value;
    }
}
