package pl.edu.agh.ctp.mcu.enums;

/**
 * Created by Leszek Placzkiewicz on 01.04.16.
 */
public enum McuMethodName {
    CONFERENCE_CREATE("conference.create"),
    CONFERENCE_DESTROY("conference.destroy"),
    CONFERENCE_END("conference.end"),
    PARTICIPANT_ADD("participant.add"),
    PARTICIPANT_ENUMERATE("participant.enumerate"),
    PARTICIPANT_FECC("participant.fecc"),
    PARTICIPANT_MESSAGE("participant.message"),
    PARTICIPANT_MODIFY("participant.modify"),
    PARTICIPANT_STATUS("participant.status"),
    CONFERENCE_ENUMERATE("conference.enumerate"),
    CONFERENCE_MODIFY("conference.modify"),
    CONFERENCE_STREAMING_MODIFY("conference.streaming.modify"),
    CONFERENCE_STREAMING_QUERY("conference.streaming.query"),
    FEEDBACK_CONFIGURE("feedbackReceiver.configure"),
    FEEDBACK_QUERY("feedbackReceiver.query");

    private final String methodName;


    McuMethodName(String methodName) {
        this.methodName = methodName;
    }

    @Override
    public String toString() {
        return methodName;
    }
}
