package pl.edu.agh.ctp.mcu;

import com.google.common.base.Preconditions;

import java.util.Map;
import java.util.Optional;

/**
 * Created by Leszek Placzkiewicz on 2016-09-17.
 */
public class McuParticipantWrapper {

    private final Map<String, Object> mcuParticipant;


    public McuParticipantWrapper(Map<String, Object> mcuParticipant) {
        this.mcuParticipant = Preconditions.checkNotNull(mcuParticipant);
    }

    public Optional<String> retrieveParticipantName() {
        return prepareStringOptional(McuResponseConstants.PARTICIPANT_NAME);
    }

    public Optional<String> retrieveConferenceName() {
        return prepareStringOptional(McuResponseConstants.CONFERENCE_NAME);
    }

    public Optional<String> retrieveAutoAttendantUniqueId() {
        return prepareStringOptional(McuResponseConstants.AUTO_ATTENDANT_UNIQUE_ID);
    }

    public Optional<String> retrieveParticipantProtocol() {
        return prepareStringOptional(McuResponseConstants.PARTICIPANT_PROTOCOL);
    }

    public Optional<String> retrieveParticipantType() {
        return prepareStringOptional(McuResponseConstants.PARTICIPANT_TYPE);
    }

    private Optional<String> prepareStringOptional(String propertyName) {
        return Optional.of(mcuParticipant.get(propertyName)).map(Object::toString);
    }
}
