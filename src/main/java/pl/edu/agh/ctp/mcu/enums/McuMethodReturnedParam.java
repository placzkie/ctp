package pl.edu.agh.ctp.mcu.enums;

/**
 * Created by Leszek Placzkiewicz on 07.04.16.
 */
public enum McuMethodReturnedParam {
    PARTICIPANTS("participants"),
    CONFERENCES("conferences");

    private final String paramName;


    McuMethodReturnedParam(String paramName) {
        this.paramName = paramName;
    }

    @Override
    public String toString() {
        return paramName;
    }
}
