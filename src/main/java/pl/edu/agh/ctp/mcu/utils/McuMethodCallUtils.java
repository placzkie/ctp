package pl.edu.agh.ctp.mcu.utils;

import pl.edu.agh.ctp.mcu.enums.McuMethodParam;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Leszek Placzkiewicz on 07.04.16.
 */
public enum McuMethodCallUtils {
    ;

    public static Map<McuMethodParam, Object> createDefaultMap(String authenticationUser,
                                                               String authenticationPassword) {
        Map<McuMethodParam, Object> params = new HashMap<>();
        params.put(McuMethodParam.AUTHENTICATION_USER, authenticationUser);
        params.put(McuMethodParam.AUTHENTICATION_PASSWORD, authenticationPassword);
        return params;
    }

    public static Map<String, Object> prepareMapToSend(Map<McuMethodParam, Object> input) {
        return input.entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue()));
    }
}
