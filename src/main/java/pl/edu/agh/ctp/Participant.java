package pl.edu.agh.ctp;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Leszek Placzkiewicz on 2016-09-16.
 */
public class Participant {

    @SerializedName("name")
    private final String name;

    @SerializedName("audioMuted")
    private final boolean audioMuted;

    @SerializedName("displayName")
    private final String displayName;

    @SerializedName("important")
    private final boolean important;

    @SerializedName("activeSpeaker")
    private final boolean activeSpeaker;

    @SerializedName("lecturer")
    private final boolean lecturer;


    //TODO - introduce builder or pass McuParticipantWrapper to constructor
    public Participant(String name,
                       boolean audioMuted,
                       String displayName,
                       boolean important,
                       boolean activeSpeaker,
                       boolean lecturer) {
        this.name = name;
        this.audioMuted = audioMuted;
        this.displayName = displayName;
        this.important = important;
        this.activeSpeaker = activeSpeaker;
        this.lecturer = lecturer;
    }

    public String getName() {
        return name;
    }

    public boolean isAudioMuted() {
        return audioMuted;
    }
}
