package pl.edu.agh.ctp.websocket;

import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.apache.log4j.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.handshake.ServerHandshakeBuilder;
import org.java_websocket.server.WebSocketServer;
import pl.edu.agh.ctp.authentication.UserValidator;
import pl.edu.agh.ctp.users.UserCredentials;
import pl.edu.agh.ctp.websocket.messages.IncomingMessage;
import pl.edu.agh.ctp.websocket.queues.IncomingMessageQueue;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * Created by Leszek Placzkiewicz on 2016-07-06.
 */
public class Server extends WebSocketServer {

    private static final Logger logger = Logger.getLogger(Server.class);


    private final MessageParser messageParser;

    private final IncomingMessageQueue incomingMessageQueue;

    private final UserValidator userValidator;

    private final BiMap<String, WebSocket> usersConnections;


    public Server(int port, IncomingMessageQueue incomingMessageQueue, UserValidator userValidator)
                                                                                    throws UnknownHostException {
        super(new InetSocketAddress(port));
        this.incomingMessageQueue = Preconditions.checkNotNull(incomingMessageQueue);
        this.userValidator = Preconditions.checkNotNull(userValidator);
        this.messageParser = new MessageParser();
        this.usersConnections = HashBiMap.create();
    }

    @Override
    public ServerHandshakeBuilder onWebsocketHandshakeReceivedAsServer(WebSocket conn,
                                                                       Draft draft,
                                                                       ClientHandshake request)
                                                                                    throws InvalidDataException {
        UserCredentials userCredentials = extractUserCredentials(request);
        if ( !userValidator.isValid(userCredentials)) {
            throw new InvalidDataException(1);
        }

        return super.onWebsocketHandshakeReceivedAsServer(conn, draft, request);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        UserCredentials userCredentials = extractUserCredentials(handshake);
        usersConnections.put(userCredentials.getUsername(), conn);
        logger.info("New user: " + userCredentials.getUsername());
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        String username = usersConnections.inverse().get(conn);
        logger.info("Removing user: " + username);
        usersConnections.inverse().remove(conn);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        logger.debug(message);
        IncomingMessage incomingMessage = messageParser.fromJson(message);
        incomingMessageQueue.offer(incomingMessage);
    }

/*
    public static void main(String[] args) throws InterruptedException , IOException {
        int port = 8887; // 843 flash policy port
        try {
            port = Integer.parseInt(args[0]);
        } catch (Exception ex) {
        }
        Server s = new Server(port);
        s.start();
        System.out.println( "ChatServer started on port: " + s.getPort() );

        BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String in = sysin.readLine();
            s.sendToAll(in);
            if(in.equals( "exit")) {
                s.stop();
                break;
            } else if(in.equals( "restart")) {
                s.stop();
                s.start();
                break;
            }
        }
    }
*/
    @Override
    public void onError( WebSocket conn, Exception ex ) {
        ex.printStackTrace();
        if( conn != null ) {
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

//    public void sendToAll( String text ) {
//        Collection<WebSocket> con = connections();
//        synchronized (con) {
//            for(WebSocket c : con) {
//                c.send(text);
//            }
//        }
//    }

    public void sendToUser(String username, String message) {
        WebSocket connection = usersConnections.get(username);
        if(connection != null) {
            synchronized (connection) {
                logger.debug("Sending message to " + username + ":" + message);
                connection.send(message);
            }
        }
    }

    private UserCredentials extractUserCredentials(ClientHandshake handshake) {
        String resourceDescriptor = handshake.getResourceDescriptor();
        int usernameIndex = resourceDescriptor.indexOf("username");
        int passwordIndex = resourceDescriptor.indexOf("password");
        String username = resourceDescriptor.substring(usernameIndex + "username".length() + 1, passwordIndex - 1);
        String password = resourceDescriptor.substring(passwordIndex + "password".length() + 1);

        return new UserCredentials(username, password);
    }
}