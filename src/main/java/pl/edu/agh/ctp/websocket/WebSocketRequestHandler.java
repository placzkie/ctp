package pl.edu.agh.ctp.websocket;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import pl.edu.agh.ctp.Conference;
import pl.edu.agh.ctp.Participant;
import pl.edu.agh.ctp.common.RequiredParticipantParameters;
import pl.edu.agh.ctp.common.RequiredParticipantParametersHelper;
import pl.edu.agh.ctp.console.IMcuFacade;
import pl.edu.agh.ctp.console.InMemoryRepository;
import pl.edu.agh.ctp.mcu.enums.Direction;
import pl.edu.agh.ctp.users.IUserRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Leszek Placzkiewicz on 2016-07-10.
 */
public class WebSocketRequestHandler {

    private final IMcuFacade mcuFacade;

    private final InMemoryRepository inMemoryRepository;

    private final IUserRepository userRepository;

    private final RequiredParticipantParametersHelper requiredParticipantParametersHelper;

    public WebSocketRequestHandler(IMcuFacade mcuFacade,
                                   InMemoryRepository inMemoryRepository,
                                   IUserRepository userRepository) {
        this.mcuFacade = Preconditions.checkNotNull(mcuFacade);
        this.inMemoryRepository = Preconditions.checkNotNull(inMemoryRepository);
        this.userRepository = Preconditions.checkNotNull(userRepository);
        this.requiredParticipantParametersHelper = new RequiredParticipantParametersHelper(inMemoryRepository);
    }

    public List<String> getUserConferences(String username) {
        return userRepository.getUserConferences(username);
    }

    public List<Participant> getConferenceParticipants(String conferenceName) {
        List<Map<String, Object>> participants = mcuFacade.enumerateParticipants();
        inMemoryRepository.setParticipants(participants);

        return participants.stream()
                .filter(participant ->
                        participant.getOrDefault("conferenceName",
                                participant.get("autoAttendantUniqueID")).equals(conferenceName))
                .map(participant ->
                        new Participant(String.valueOf(participant.get("participantName")),
                            Boolean.valueOf(
                            ((Map<String, Object>) participant.get("currentState")).get("audioRxMuted").toString()),
                                ((Map<String, Object>) participant.get("currentState")).get("displayName").toString(),
                            Boolean.valueOf(
                                ((Map<String, Object>) participant.get("currentState")).get("important").toString()),
                            Boolean.valueOf(
                                ((Map<String, Object>) participant.get("currentState")).get("activeSpeaker").toString()),
                            Boolean.valueOf(
                                ((Map<String, Object>) participant.get("currentState")).get("lecturer").toString())
                                ))
                .collect(Collectors.toList());
    }

    public void handleSetImportantParticipant(String participantName) {
        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);

        if (!requiredParticipantParameters.isPresent()) {
            throw new RuntimeException("Participant " + participantName + "doesn't exist");
        }

        Map<String, Object> importantParticipantParameter = Maps.newHashMap();
        importantParticipantParameter.put("important", true);

        mcuFacade.modifyParticipant(requiredParticipantParameters.get(), importantParticipantParameter);
    }

    public void handleSetFocusType(String conferenceName, String focusType, String participantName) {
        Map<String, Object> focusPatameters = Maps.newHashMap();
        focusPatameters.put("focusType", focusType);
        if (participantName != null) {
            //if focusType == Participant, fill participantFocus parameter
            Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);

            if (!requiredParticipantParameters.isPresent()) {
                throw new RuntimeException("Participant " + participantName + "doesn't exist");
            }

            Map<String, Object> focusParticipantParameter = Maps.newHashMap();
            focusParticipantParameter.put("participantName", participantName);
            focusParticipantParameter.put("participantProtocol",
                                                        requiredParticipantParameters.get().getParticipantProtocol());
            focusParticipantParameter.put("participantType", requiredParticipantParameters.get().getParticipantType());

            focusPatameters.put("focusParticipant", focusParticipantParameter);
        }

        mcuFacade.modifyConferenceStreaming(conferenceName, focusPatameters);
    }

    public void handleLayoutConference(String conferenceName, String cpLayout) {
        Map<String, Object> cpLayoutParameter = Maps.newHashMap();
        cpLayoutParameter.put("cpLayout", cpLayout);

        mcuFacade.modifyConferenceStreaming(conferenceName, cpLayoutParameter);
    }

    public void handleFeccParticipant(String participantName, Direction direction) {
        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);
        if (!requiredParticipantParameters.isPresent()) {
            throw new RuntimeException("Participant " + participantName + "doesn't exist");
        }

        mcuFacade.feccParticipant(requiredParticipantParameters.get(), direction);
    }

    public void handleLayoutParticipant(String participantName, int layoutNumber) {
        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);
        if (!requiredParticipantParameters.isPresent()) {
            throw new RuntimeException("Participant " + participantName + "doesn't exist");
        }

        Map<String, Object> cpLayoutParameter = new HashMap<>();
        cpLayoutParameter.put("cpLayout", "layout" + layoutNumber);

        mcuFacade.modifyParticipant(requiredParticipantParameters.get(), cpLayoutParameter);
    }

    public void handleMuteParticipant(String participantName, boolean mute) {
        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);
        if (!requiredParticipantParameters.isPresent()) {
            throw new RuntimeException("Participant " + participantName + "doesn't exist");
        }

        Map<String, Object> muteParameter = new HashMap<>();
        muteParameter.put("audioRxMuted", mute);
        mcuFacade.modifyParticipant(requiredParticipantParameters.get(), muteParameter);
    }

    public void handleMessageParticipant(String participantName, String message) {
        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);
        if (!requiredParticipantParameters.isPresent()) {
            throw new RuntimeException("Participant " + participantName + " doesn't exist");
        }

        mcuFacade.sendMessage(requiredParticipantParameters.get(), message);
    }

    public void handleEnumerateConferences() {
        List<Map<String, Object>> conferences = mcuFacade.enumerateConferences();
        inMemoryRepository.setConferences(conferences);
        conferences.stream()
                    .map(conference -> conference.getOrDefault("conferenceName", "NO_NAME"))
                    .forEach(System.out::println);
    }

    public void handleEnumerateParticipants() {
        List<Map<String, Object>> participants = mcuFacade.enumerateParticipants();
        inMemoryRepository.setParticipants(participants);
        //TODO - its ugly
        participants.stream().map(participant -> participant.getOrDefault("participantName", "NO_NAME") + " " +
                participant.getOrDefault("conferenceName", "NO_CONFERENCE") + " " +
                ((Map<String, Object>) participant.get("currentState")).getOrDefault("address", "NO_ADDRESS"))
                .forEach(System.out::println);
    }

    public Conference getConferenceConfig(String conferenceName) {
        Map<String, Object> conference = mcuFacade.queryConferenceStreaming(conferenceName);
        String focusType = (String) conference.get("focusType");

        Object focusParticipant = conference.getOrDefault("focusParticipant", null);
        String focusParticipantName = null;
        if (focusParticipant != null) {
            //TODO - so ugly
            focusParticipantName = (String)((Map<String, Object>) focusParticipant).get("participantName");
        }

        int currentLayout = (int) conference.get("currentLayout");
        String layoutSource = (String) conference.get("layoutSource");

        return new Conference(conferenceName, focusType, focusParticipantName, currentLayout, layoutSource);
    }
}


