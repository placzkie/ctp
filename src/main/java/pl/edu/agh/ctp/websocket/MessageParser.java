package pl.edu.agh.ctp.websocket;

import com.google.gson.Gson;
import pl.edu.agh.ctp.websocket.messages.IncomingMessage;
import pl.edu.agh.ctp.websocket.messages.OutgoingMessage;

/**
 * Created by Leszek Placzkiewicz on 2016-07-09.
 */
public class MessageParser {

    private final Gson gson;


    public MessageParser() {
        this.gson = new Gson();
    }

    public IncomingMessage fromJson(String incomingMessage) {
        return gson.fromJson(incomingMessage, IncomingMessage.class);
    }

    public String toJson(OutgoingMessage outgoingMessage) {
        return gson.toJson(outgoingMessage);
    }
}
