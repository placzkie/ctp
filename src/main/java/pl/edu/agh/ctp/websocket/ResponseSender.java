package pl.edu.agh.ctp.websocket;

import com.google.common.base.Preconditions;
import pl.edu.agh.ctp.websocket.messages.OutgoingMessage;
import pl.edu.agh.ctp.websocket.queues.OutgoingMessageQueue;

/**
 * Created by Leszek Placzkiewicz on 2016-08-07.
 */
public class ResponseSender implements Runnable {

    private final OutgoingMessageQueue outgoingMessageQueue;

    private final Server server;

    private final MessageParser messageParser;


    public ResponseSender(OutgoingMessageQueue outgoingmessageQueue, Server server) {
        this.outgoingMessageQueue = Preconditions.checkNotNull(outgoingmessageQueue);
        this.server = Preconditions.checkNotNull(server);
        this.messageParser = new MessageParser();
    }

    @Override
    public void run() {
        while (true) {
            try {
                OutgoingMessage message = outgoingMessageQueue.take();
                System.out.println("Sending response");
                sendResponse(message);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void sendResponse(OutgoingMessage message) {
        String username = message.getUsername();
        String jsonMessage = messageParser.toJson(message);

        server.sendToUser(username, jsonMessage);
    }
}
