package pl.edu.agh.ctp.websocket.queues;

import com.google.common.collect.Queues;
import pl.edu.agh.ctp.websocket.messages.IncomingMessage;

import java.util.concurrent.BlockingQueue;

/**
 * Created by Leszek Placzkiewicz on 2016-07-09.
 */
public class IncomingMessageQueue {

    private static final int QUEUE_SIZE = 100;


    private final BlockingQueue<IncomingMessage> queue;


    public IncomingMessageQueue() {
        queue = Queues.newArrayBlockingQueue(QUEUE_SIZE);
    }

    public void offer(IncomingMessage incomingMessage) {
        queue.offer(incomingMessage);
    }

    public IncomingMessage take() throws InterruptedException {
        return queue.take();
    }
}
