package pl.edu.agh.ctp.websocket;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import pl.edu.agh.ctp.Conference;
import pl.edu.agh.ctp.Participant;
import pl.edu.agh.ctp.mcu.enums.Direction;
import pl.edu.agh.ctp.users.UserCredentials;
import pl.edu.agh.ctp.websocket.enums.IncomingMessageType;
import pl.edu.agh.ctp.websocket.enums.OutgoingMessageType;
import pl.edu.agh.ctp.websocket.messages.IncomingMessage;
import pl.edu.agh.ctp.websocket.messages.OutgoingMessage;
import pl.edu.agh.ctp.websocket.queues.IncomingMessageQueue;
import pl.edu.agh.ctp.websocket.queues.OutgoingMessageQueue;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Leszek Placzkiewicz on 2016-07-10.
 */
public class RequestManager implements Runnable {

    private final IncomingMessageQueue incomingMessageQueue;

    private final WebSocketRequestHandler webSocketRequestHandler;

    private final OutgoingMessageQueue outgoingMessageQueue;


    public RequestManager(IncomingMessageQueue incomingMessageQueue,
                          WebSocketRequestHandler webSocketRequestHandler,
                          OutgoingMessageQueue outgoingMessageQueue) {
        this.incomingMessageQueue = Preconditions.checkNotNull(incomingMessageQueue);
        this.webSocketRequestHandler = Preconditions.checkNotNull(webSocketRequestHandler);
        this.outgoingMessageQueue = Preconditions.checkNotNull(outgoingMessageQueue);
    }

    public void run() {
        while (true) {
            try {
                IncomingMessage message = incomingMessageQueue.take();
                handleRequest(message);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void handleRequest(IncomingMessage incomingMessage) {
        String recipient = incomingMessage.getRecipient();
        IncomingMessageType incomingMessageType = incomingMessage.getIncomingMessageType();

        switch (incomingMessageType) {
            case SET_CONFERENCE_LAYOUT:
                handleSetConferenceLayout(incomingMessage, webSocketRequestHandler);
                break;
            case SET_IMPORTANT_PARTICIPANT:
                handleSetImportantParticipant(incomingMessage, webSocketRequestHandler);
                break;
            case SET_FOCUS_TYPE:
                handleSetFocusType(incomingMessage, webSocketRequestHandler);
                break;
            case GET_CONFERENCE_CONFIG:
                handleGetConferenceConfig(incomingMessage, webSocketRequestHandler, outgoingMessageQueue);
                break;
            case GET_CONFERENCES:
                handleGetConferences(incomingMessage, webSocketRequestHandler, outgoingMessageQueue);
                break;
            case GET_PARTICIPANTS:
                handleGetParticipants(incomingMessage, webSocketRequestHandler, outgoingMessageQueue);
                break;
            case MOVE_CAMERA_LEFT:
                webSocketRequestHandler.handleFeccParticipant(recipient, Direction.LEFT);
                break;
            case MOVE_CAMERA_RIGHT:
                webSocketRequestHandler.handleFeccParticipant(recipient, Direction.RIGHT);
                break;
            case MOVE_CAMERA_DOWN:
                webSocketRequestHandler.handleFeccParticipant(recipient, Direction.DOWN);
                break;
            case MOVE_CAMERA_UP:
                webSocketRequestHandler.handleFeccParticipant(recipient, Direction.UP);
                break;
            case ZOOM_CAMERA_IN:
                webSocketRequestHandler.handleFeccParticipant(recipient, Direction.ZOOM_IN);
                break;
            case ZOOM_CAMERA_OUT:
                webSocketRequestHandler.handleFeccParticipant(recipient, Direction.ZOOM_OUT);
                break;
            case CHANGE_LAYOUT:
                int layoutNumber = extractIntProperty("layout_number", incomingMessage);
                webSocketRequestHandler.handleLayoutParticipant(recipient, layoutNumber);
                break;
            case SEND_MESSAGE:
                String message = extractStringProperty("message_text", incomingMessage);
                webSocketRequestHandler.handleMessageParticipant(recipient, message);
                break;
            case TURN_MICROPHONE_OFF:
                webSocketRequestHandler.handleMuteParticipant(recipient, true);
                break;
            case TURN_MICROPHONE_ON:
                webSocketRequestHandler.handleMuteParticipant(recipient, false);
                break;
            case ENUMERATE_CONFERENCES:
                //TODO - this will probably be deleted
                webSocketRequestHandler.handleEnumerateConferences();
                break;
            case ENUMERATE_PARTICIPANTS:
                //TODO - pass conferenceName as argument, this should probably return list of participants instead of printing it out
                webSocketRequestHandler.handleEnumerateParticipants();
                break;
        }
    }

    private UserCredentials extractUserCredentials(Map<String, String> additionalProperties) {
        Optional<String> usernameOptional = Optional.ofNullable(additionalProperties.get("username"));
        Optional<String> passwordOptional = Optional.ofNullable(additionalProperties.get("password"));

        String username = usernameOptional.orElseThrow(() -> new RuntimeException("Missing username"));
        String password = passwordOptional.orElseThrow(() -> new RuntimeException("Missing password"));

        return new UserCredentials(username, password);
    }

    private int extractIntProperty(String propertyName, IncomingMessage incomingMessage) {
        String stringPropertyValue = extractStringProperty(propertyName, incomingMessage);
        return Integer.valueOf(stringPropertyValue);
    }

    private String extractStringProperty(String propertyName, IncomingMessage incomingMessage) {
        Optional<String> stringOptional = Optional.ofNullable(incomingMessage.getProperty(propertyName));
        return stringOptional.orElseThrow(() -> new RuntimeException("Missing" + propertyName + "property"));
    }

    private void handleGetConferenceConfig(IncomingMessage incomingMessage,
                                           WebSocketRequestHandler webSocketRequestHandler,
                                           OutgoingMessageQueue outgoingMessageQueue) {
        String username = extractStringProperty("username", incomingMessage);
        String conferenceName = extractStringProperty("conferenceName", incomingMessage);
        Conference conference = webSocketRequestHandler.getConferenceConfig(conferenceName);

        Map<String, Object> responseProperties = Maps.newHashMap();
        responseProperties.put("conference", conference);

        OutgoingMessage outgoingMessage =
                new OutgoingMessage(OutgoingMessageType.GET_CONFERENCE_CONFIG, username, responseProperties);
        outgoingMessageQueue.offer(outgoingMessage);
    }

    private void handleGetConferences(IncomingMessage incomingMessage,
                                      WebSocketRequestHandler webSocketRequestHandler,
                                      OutgoingMessageQueue outgoingMessageQueue) {
        String username = extractStringProperty("username", incomingMessage);
        List<String> conferences = webSocketRequestHandler.getUserConferences(username);

        Map<String, Object> responseProperties = Maps.newHashMap();
        responseProperties.put("conferences", conferences);

        OutgoingMessage outgoingMessage =
                new OutgoingMessage(OutgoingMessageType.GET_CONFERENCES, username, responseProperties);
        outgoingMessageQueue.offer(outgoingMessage);
    }

    private void handleGetParticipants(IncomingMessage incomingMessage,
                                       WebSocketRequestHandler webSocketRequestHandler,
                                       OutgoingMessageQueue outgoingMessageQueue) {
        String conferenceName = extractStringProperty("conferenceName", incomingMessage);
        List<Participant> participants = webSocketRequestHandler.getConferenceParticipants(conferenceName);

        String username = extractStringProperty("username", incomingMessage);

        Map<String, Object> responseProperties = Maps.newHashMap();
        responseProperties.put("participants", participants);

        OutgoingMessage outgoingMessage =
                new OutgoingMessage(OutgoingMessageType.GET_PARTICIPANTS, username, responseProperties);
        outgoingMessageQueue.offer(outgoingMessage);
    }

    private void handleSetFocusType(IncomingMessage incomingMessage, WebSocketRequestHandler webSocketRequestHandler) {
        String conferenceName = extractStringProperty("conferenceName", incomingMessage);
        String focusType = extractStringProperty("focusType", incomingMessage);
        String participantName = null;
        if ("participant".equals(focusType)) {
            participantName = extractStringProperty("participantName", incomingMessage);
        }
        webSocketRequestHandler.handleSetFocusType(conferenceName, focusType, participantName);
    }

    private void handleSetImportantParticipant(IncomingMessage incomingMessage,
                                               WebSocketRequestHandler webSocketRequestHandler) {
        String participantName = extractStringProperty("participantName", incomingMessage);
        webSocketRequestHandler.handleSetImportantParticipant(participantName);
    }

    private void handleSetConferenceLayout(IncomingMessage incomingMessage,
                                           WebSocketRequestHandler webSocketRequestHandler) {
        String conferenceName = extractStringProperty("conferenceName", incomingMessage);
        String cpLayout = extractStringProperty("cpLayout", incomingMessage);

        webSocketRequestHandler.handleLayoutConference(conferenceName, cpLayout);
    }

}
