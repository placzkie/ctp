package pl.edu.agh.ctp.websocket.enums;

/**
 * Created by Leszek Placzkiewicz on 2016-08-07.
 */
public enum OutgoingMessageType {
    GET_CONFERENCES,
    GET_PARTICIPANTS,

    GET_CONFERENCE_CONFIG,

    CONFERENCE_STATE
}
