package pl.edu.agh.ctp.websocket.queues;

import com.google.common.collect.Queues;
import pl.edu.agh.ctp.websocket.messages.OutgoingMessage;

import java.util.concurrent.BlockingQueue;

/**
 * Created by Leszek Placzkiewicz on 2016-08-07.
 */
public class OutgoingMessageQueue {

    private static final int QUEUE_SIZE = 100;


    private final BlockingQueue<OutgoingMessage> queue;


    public OutgoingMessageQueue() {
        queue = Queues.newArrayBlockingQueue(QUEUE_SIZE);
    }

    public void offer(OutgoingMessage outgoingMessage) {
        queue.offer(outgoingMessage);
    }

    public OutgoingMessage take() throws InterruptedException {
        return queue.take();
    }
}
