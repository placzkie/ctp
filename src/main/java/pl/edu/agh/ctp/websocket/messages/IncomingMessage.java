package pl.edu.agh.ctp.websocket.messages;

import com.google.common.base.Preconditions;
import com.google.gson.annotations.SerializedName;
import pl.edu.agh.ctp.websocket.enums.IncomingMessageType;

import java.util.Collections;
import java.util.Map;

/**
 * Created by Leszek Placzkiewicz on 2016-07-09.
 */
public class IncomingMessage {

    @SerializedName("messageType")
    private final IncomingMessageType incomingMessageType;

    @SerializedName("recipient")
    private final String recipient;

    @SerializedName("additionalProperties")
    private final Map<String, String> additionalProperties;


    public IncomingMessage(IncomingMessageType incomingMessageType,
                           String recipient,
                           Map<String, String> additionalProperties) {
        this.incomingMessageType = Preconditions.checkNotNull(incomingMessageType);
        this.recipient = Preconditions.checkNotNull(recipient);
        this.additionalProperties = Preconditions.checkNotNull(additionalProperties);
    }

    public IncomingMessageType getIncomingMessageType() {
        return incomingMessageType;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getProperty(String name) {
        return additionalProperties.get(name);
    }

    public Map<String, String> getAdditionalProperties() {
        return Collections.unmodifiableMap(additionalProperties);
    }

    @Override
    public String toString() {
        return "IncomingMessage{" +
                "incomingMessageType=" + incomingMessageType +
                ", recipient='" + recipient + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
