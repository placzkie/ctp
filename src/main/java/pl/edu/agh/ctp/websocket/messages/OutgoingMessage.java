package pl.edu.agh.ctp.websocket.messages;

import com.google.common.base.Preconditions;
import com.google.gson.annotations.SerializedName;
import pl.edu.agh.ctp.websocket.enums.OutgoingMessageType;

import java.util.Map;

/**
 * Created by Leszek Placzkiewicz on 2016-08-07.
 */
public class OutgoingMessage {

    @SerializedName("messageType")
    private final OutgoingMessageType messageType;

    @SerializedName("username")
    private final String username;

    @SerializedName("additionalProperties")
    private final Map<String, Object> additionalProperties;


    public OutgoingMessage(OutgoingMessageType messageType, String username, Map<String, Object> additionalProperties) {
        this.messageType = Preconditions.checkNotNull(messageType);
        this.username = Preconditions.checkNotNull(username);
        this.additionalProperties = Preconditions.checkNotNull(additionalProperties);
    }

    public OutgoingMessageType getMessageType() {
        return messageType;
    }

    public String getUsername() {
        return username;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }
}
