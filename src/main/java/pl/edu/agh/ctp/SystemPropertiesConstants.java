package pl.edu.agh.ctp;

/**
 * Created by Leszek Placzkiewicz on 2016-09-16.
 */
public class SystemPropertiesConstants {

    public static final String AUTHENTICATION_USER = "authenticationUser";

    public static final String AUTHENTICATION_PASSWORD = "authenticationPassword";

}
