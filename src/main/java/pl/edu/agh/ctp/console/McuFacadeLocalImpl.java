package pl.edu.agh.ctp.console;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import pl.edu.agh.ctp.common.RequiredParticipantParameters;
import pl.edu.agh.ctp.feedback.McuImitator;
import pl.edu.agh.ctp.mcu.enums.Direction;

import java.util.*;

/**
 * Created by Leszek Placzkiewicz on 2016-07-10.
 */
public class McuFacadeLocalImpl implements IMcuFacade {

    private static int NUMBER = 0;


    private final McuImitator mcuImitator;


    public McuFacadeLocalImpl() {
        this(McuImitator.DEFAULT_IMITATOR);
    }

    public McuFacadeLocalImpl(McuImitator mcuImitator) {
        this.mcuImitator = mcuImitator;
    }

    @Override
    public List<Map<String, Object>> enumerateConferences() {
        return populateConferences();
    }

    @Override
    public List<Map<String, Object>> enumerateParticipants() {
        return populateParticipants();
    }

    @Override
    public void feccParticipant(RequiredParticipantParameters requiredParticipantParameters, Direction direction) {
        System.out.println("FECC_PARTICIPANT: " + requiredParticipantParameters + " " + direction);
    }

    @Override
    public void sendMessage(RequiredParticipantParameters requiredParticipantParameters, String message) {
        System.out.println("SEND_MESSAGE: " + requiredParticipantParameters + " " + message);
    }

    @Override
    public void modifyParticipant(RequiredParticipantParameters requiredParticipantParameters,
                                  Map<String, Object> optionalParameters) {
        System.out.println("MODIFY_PARTICIPANT: " + requiredParticipantParameters + " " + optionalParameters);
        mcuImitator.updateClient(Sets.newHashSet("importanceChanged"));
    }

    @Override
    public void modifyConference(String conferenceName, Map<String, Object> optionalParameters) {
        System.out.println("MODIFY_CONFERENCE: " + conferenceName + " " + optionalParameters);
    }

    @Override
    public void registerFeedback(String address) {
        System.out.println("REGISTER_FEEDBACK: " + address);
    }

    @Override
    public void addParticipant(String conferenceName, String participantName, String address) {
        throw new UnsupportedOperationException("This method probably will not be needed");
    }

    @Override
    public Map<String, Object> queryConferenceStreaming(String conferenceName) {
        switch (conferenceName) {
            case "conf1":
                return prepareConf1Config();
            case "conf2":
                return prepareConf2Config();
            case "conf3":
                return prepareConf3Config();
            default:
                throw new RuntimeException("Illegal conference name");
        }
    }

    @Override
    public void modifyConferenceStreaming(String conferenceName, Map<String, Object> optionalParameters) {
        System.out.println("MODIFY CONFERENCE STREAMING " + conferenceName + optionalParameters);
        mcuImitator.updateClient(Sets.newHashSet("conferenceConfigurationChanged"));
    }

    private Map<String, Object> prepareConf1Config() {
        Map<String, Object> conference = Maps.newHashMap();
        conference.put("currentLayout", 1);
        conference.put("layoutSource", "conferenceCustom");
        conference.put("focusType", "participant");

        Map<String, Object> focusParticipant = Maps.newHashMap();
        focusParticipant.put("participantName", "participant3");
        conference.put("focusParticipant", focusParticipant);

        return conference;
    }

    private Map<String, Object> prepareConf2Config() {
        Map<String, Object> conference = Maps.newHashMap();
        conference.put("currentLayout", 5);
        conference.put("layoutSource", "familyx");
        conference.put("focusType", "voiceActivated");

        return conference;
    }

    private Map<String, Object> prepareConf3Config() {
        Map<String, Object> conference = Maps.newHashMap();
        conference.put("currentLayout", 3);
        conference.put("layoutSource", "familyx");
        conference.put("focusType", "h239");

        return conference;
    }

    private List<Map<String, Object>> populateConferences() {
        List<Map<String, Object>> conferences = new ArrayList<>();
        List<String> conferenceNames = Arrays.asList("conf1", "conf2", "conf3");
        for (String conferenceName : conferenceNames) {
            Map<String, Object> map = new HashMap<>();
            map.put("conferenceName", conferenceName);
            map.put("conferenceActive", true);
            conferences.add(map);
        }

        return conferences;
    }

    private List<Map<String, Object>> populateParticipants() {
        List<Map<String, Object>> conferences = new ArrayList<>();
        Map<String, String> participantToConference = new LinkedHashMap<>();
        participantToConference.put("participant1", "conf2");
        participantToConference.put("participant2", "conf2");
        participantToConference.put("participant3", "conf1");
        participantToConference.put("participant4", "conf3");
        participantToConference.put("participant5", "conf2");
        participantToConference.put("participant6", "conf2");
        participantToConference.put("participant7", "conf2");
        participantToConference.put("participant8", "conf2");
        participantToConference.put("participant9", "conf2");


        if (NUMBER++ % 2 == 0) {
            participantToConference.put("participant10", "conf2");
            participantToConference.put("participant11", "conf2");
            participantToConference.put("participant12", "conf2");
            participantToConference.put("participant13", "conf2");
            participantToConference.put("participant14", "conf2");
            participantToConference.put("participant15", "conf2");
            participantToConference.put("participant16", "conf2");
            participantToConference.put("participant17", "conf2");
            participantToConference.put("participant18", "conf2");
            participantToConference.put("participant18", "conf2");
        }

        int i = 0;
        for (Map.Entry<String, String> entry : participantToConference.entrySet()) {
            Map<String, Object> map = new HashMap<>();
            map.put("participantName", entry.getKey());
            map.put("conferenceName", entry.getValue());
            map.put("currentState", prepareCurrentState(i % 4 == 0,
                                                        prepareDisplayName(entry.getKey()),
                                                        (i +1) % 15 == 0,
                                                        i % 5 == 0,
                                                        i % 15 == 0));
            map.put("participantProtocol", "h323");
            map.put("participantType", "by_address");

            conferences.add(map);
            i++;
        }

        return conferences;
    }

    private String prepareDisplayName(String name) {
        return name.substring(0, 4) + name.charAt(name.length() - 1);
    }

    private Map<String, Object> prepareCurrentState(boolean audioRxMuted,
                                                    String displayName,
                                                    boolean important,
                                                    boolean activeSpeaker,
                                                    boolean lecturer) {
        Map<String, Object> currentState = Maps.newHashMap();
        currentState.put("audioRxMuted", audioRxMuted);
        currentState.put("displayName", displayName);
        currentState.put("important", important);
        currentState.put("activeSpeaker", activeSpeaker);
        currentState.put("lecturer", lecturer);

        return currentState;
    }
}
