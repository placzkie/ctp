package pl.edu.agh.ctp.console;

import com.google.common.collect.Lists;
import pl.edu.agh.ctp.McuConstants;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Leszek Placzkiewicz on 2016-04-27.
 */
public class InMemoryRepository {

    private final List<Map<String, Object>> participants;

    private final List<Map<String, Object>> conferences;


    public InMemoryRepository() {
        participants = Lists.newArrayList();
        conferences = Lists.newArrayList();
    }

    public void setParticipants(List<Map<String, Object>> participantsList) {
        participants.clear();
        participants.addAll(participantsList);
    }

    public void setConferences(List<Map<String, Object>> conferencesList) {
        conferences.clear();
        conferences.addAll(conferencesList);
    }

    public Optional<Map<String, Object>> getParticipant(String participantName) {
        return participants.stream()
                            .filter(p -> p.get(McuConstants.KEY_PARTICIPANT_NAME).equals(participantName))
                            .findAny();
    }

    public List<Map<String, Object>> getConferences() {
        return conferences;
    }
}
