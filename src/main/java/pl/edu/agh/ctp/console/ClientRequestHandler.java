package pl.edu.agh.ctp.console;

import com.google.common.base.Preconditions;
import pl.edu.agh.ctp.common.RequiredParticipantParameters;
import pl.edu.agh.ctp.common.RequiredParticipantParametersHelper;
import pl.edu.agh.ctp.mcu.enums.Direction;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Created by Leszek Placzkiewicz on 2016-04-29.
 */
public class ClientRequestHandler {

    private final Scanner scanner;

    private final IMcuFacade mcuFacade;

    private final InMemoryRepository inMemoryRepository;

    private final RequiredParticipantParametersHelper requiredParticipantParametersHelper;


    public ClientRequestHandler(String authenticationUser, String authenticationPassword) {
        Preconditions.checkNotNull(authenticationUser);
        Preconditions.checkNotNull(authenticationPassword);

        this.scanner = new Scanner(System.in);
//        this.mcuFacade = new McuFacade(authenticationUser, authenticationPassword);
        this.mcuFacade = new McuFacadeLocalImpl();
        this.inMemoryRepository = new InMemoryRepository();
        this.requiredParticipantParametersHelper = new RequiredParticipantParametersHelper(inMemoryRepository);
    }

    public void handleLayoutConference() {
        System.out.print("Insert conference name: ");
        String conferenceName = readUserInput();

        while (true) {
            System.out.println("Insert layout number[1-59] or [X] to exit.");
            String layoutNumber = readUserInput();

            if (layoutNumber.equals("x")) {
                break;
            }

            Map<String, Object> customLayoutParameter = new HashMap<>();
            customLayoutParameter.put("customLayout", Integer.valueOf(layoutNumber));

            mcuFacade.modifyConference(conferenceName, customLayoutParameter);
        }
    }

    public void handleLayoutParticipant() {
        System.out.print("Insert participant name: ");
        String participantName = readUserInput();

        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);
        if (!requiredParticipantParameters.isPresent()) {
            System.out.println("Participant with given name doesn't exist.");
            return;
        }

        while (true) {
            System.out.println("Insert layout number[1-59] or [X] to exit.");
            String layoutNumber = readUserInput();

            if (layoutNumber.equals("x")) {
                break;
            }

            Map<String, Object> cpLayoutParameter = new HashMap<>();
            cpLayoutParameter.put("cpLayout", "layout" + layoutNumber);

            mcuFacade.modifyParticipant(requiredParticipantParameters.get(), cpLayoutParameter);
        }
    }

    public void handleFocusParticipant() {
        System.out.print("Insert participant name: ");
        String participantName = readUserInput();

        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);
        if (!requiredParticipantParameters.isPresent()) {
            System.out.println("Participant with given name doesn't exist.");
            return;
        }

        System.out.println("Insert focus type value: [participant], [voiceActivated], [h239] or [X] to exit.");
        String focusType = readUserInput();

        if (focusType.toLowerCase().equals("x")) {
            return;
        }

        Map<String, Object> focusTypeParameter = new HashMap<>();
        focusTypeParameter.put("focusType", focusType);
        if (focusType.equals("participant")) {
            System.out.print("Insert name of participant to display in the largest pane: ");
            String largestPaneParticipant = readUserInput();
            Optional<RequiredParticipantParameters> largestPaneParticipantParameters =
                    requiredParticipantParametersHelper.obtainRequiredParticipantParameters(largestPaneParticipant);

            if (!largestPaneParticipantParameters.isPresent()) {
                System.out.println("Participant with given name doesn't exist.");
                return;
            }

            Map<String, Object> focusParticipantMap = new HashMap<>();
            focusParticipantMap.put("participantName", largestPaneParticipantParameters.get().getParticipantName());
            focusParticipantMap.put("participantProtocol", largestPaneParticipantParameters.get().getParticipantProtocol().toString());
            focusParticipantMap.put("participantType", largestPaneParticipantParameters.get().getParticipantType().toString());

            focusTypeParameter.put("focusParticipant", focusParticipantMap);
        }

        mcuFacade.modifyParticipant(requiredParticipantParameters.get(), focusTypeParameter);
    }

    public void handleMuteParticipant() {
        System.out.print("Insert participant name: ");
        String participantName = readUserInput();

        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);
        if (!requiredParticipantParameters.isPresent()) {
            System.out.println("Participant with given name doesn't exist.");
            return;
        }

        loop:
        while (true) {
            System.out.println("Type [ON] or [OFF] or [X] to exit.");
            String option = readUserInput();

            Map<String, Object> muteParameter = new HashMap<>();
            switch (option.toLowerCase()) {
                case "x":
                    break loop;
                case "on":
                    muteParameter.put("audioRxMuted", false/*"false"*/);
                    break;
                case "off":
                    muteParameter.put("audioRxMuted", true/*"true"*/);
                    break;
                default:
                    System.out.println("Incorrect option, try again");
            }

            mcuFacade.modifyParticipant(requiredParticipantParameters.get(), muteParameter);
        }
    }

    public void handleModifyParticipant() {
        System.out.print("Insert participant name: ");
        String participantName = readUserInput();

        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);

        if (!requiredParticipantParameters.isPresent()) {
            System.out.println("Participant with given name doesn't exist.");
            return;
        }

        while (true) {
            System.out.println("Insert name of parameter to modify or [X] to exit.");
            String parameterName = readUserInput();
            if (parameterName.toLowerCase().equals("x")) {
                break;
            }

            System.out.print("Insert value of parameter: ");
            String parameterValue = readUserInput();

            Map<String, Object> optionalParameters = new HashMap<>();
            optionalParameters.put(parameterName, parameterValue);

            mcuFacade.modifyParticipant(requiredParticipantParameters.get(), optionalParameters);
        }
    }

    public void handleMessageParticipant() {
        System.out.print("Insert participant name: ");
        String participantName = readUserInput();

        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);
        if (!requiredParticipantParameters.isPresent()) {
            System.out.println("Participant with given name doesn't exist");
            return;
        }

        while (true) {
            System.out.println("Insert message to send or [X] to exit.");
            String message = readUserInput();

            if (message.toLowerCase().equals("x")) {
                break;
            }

            //TODO - add optional parameters i.e verticalPosition and durationSeconds
            mcuFacade.sendMessage(requiredParticipantParameters.get(), message);
        }
    }

    public void handleEnumerateConferences() {
        List<Map<String, Object>> conferences = mcuFacade.enumerateConferences();
        inMemoryRepository.setConferences(conferences);
        conferences.stream().map(conference -> conference.getOrDefault("conferenceName", "NO_NAME")).forEach(System.out::println);
    }

    public void handleEnumerateParticipants() {
        List<Map<String, Object>> participants = mcuFacade.enumerateParticipants();
        inMemoryRepository.setParticipants(participants);
        //TODO - its ugly
        participants.stream().map(participant -> participant.getOrDefault("participantName", "NO_NAME") + " " +
                participant.getOrDefault("conferenceName", "NO_CONFERENCE") + " " +
                ((Map<String, Object>) participant.get("currentState")).getOrDefault("address", "NO_ADDRESS"))
                .forEach(System.out::println);
    }

    public void handleFeccParticipant() {
        System.out.print("Insert participant name: ");
        String participantName = readUserInput();

        Optional<RequiredParticipantParameters> requiredParticipantParameters =
                            requiredParticipantParametersHelper.obtainRequiredParticipantParameters(participantName);
        if (!requiredParticipantParameters.isPresent()) {
            System.out.println("Participant with given name doesn't exist");
            return;
        }

        Direction direction = null;

        loop:
        while (true) {
            System.out.println("Insert direction(UP[U], DOWN[D], LEFT[L], RIGHT[R], ZOOM_IN[ZI], ZOOM_OUT[ZO], FOCUS_IN[FI], FOCUS_OUT[FO]. Or [X] to exit.)");
            String directionString = readUserInput();
            switch (directionString.toLowerCase()) {
                case "u":
                    direction = Direction.UP;
                    break;
                case "d":
                    direction = Direction.DOWN;
                    break;
                case "l":
                    direction = Direction.LEFT;
                    break;
                case "r":
                    direction = Direction.RIGHT;
                    break;
                case "zi":
                    direction = Direction.ZOOM_IN;
                    break;
                case "zo":
                    direction = Direction.ZOOM_OUT;
                    break;
                case "fi":
                    direction = Direction.FOCUS_IN;
                    break;
                case "fo":
                    direction = Direction.FOCUS_OUT;
                    break;
                case "x":
                    break loop;
                default:
                    System.out.println("Incorrect input. Try again.");
                    break;
            }

            mcuFacade.feccParticipant(requiredParticipantParameters.get(), direction);
        }
    }

    public void handleAddParticipant() {
        System.out.println("Insert conference name: ");
        String conferenceName = readUserInput();
        System.out.println("Insert participant name: ");
        String participantName = readUserInput();
        System.out.println("Insert address: ");
        String address = readUserInput();
        mcuFacade.addParticipant(conferenceName, participantName, address);
    }

    public void handleRegisterFeedback() {
        try {
            String hostAddress = Inet4Address.getLocalHost().getHostAddress();
            String feedbackAddress = "http://" + hostAddress + ":8080/RPC2";
            System.out.println("Registering feedback for: " + feedbackAddress);
            mcuFacade.registerFeedback(feedbackAddress);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    public void handleGetParticipantConfiguration() {
        System.out.print("Insert participant name: ");
        String participantName = readUserInput();

        Optional<Map<String, Object>> participantOptional = inMemoryRepository.getParticipant(participantName);
        if (!participantOptional.isPresent()) {
            System.out.println("Participant with given name doesn't exist.");
            return;
        }

        Map<String, Object> participant = participantOptional.get();
        Map<String, Object> currentState = (Map<String, Object>) participant.get("currentState");

        while (true) {
            System.out.println("Insert configuration parameter name or [X] to exit.");
            String configurationParameter = readUserInput();

            if (configurationParameter.toLowerCase().equals("x")) {
                break;
            }

            Object value = currentState.get(configurationParameter);
            System.out.println(configurationParameter + " : " + value);
        }
    }

    private String readUserInput() {
        return scanner.nextLine();
    }
}