package pl.edu.agh.ctp.console;

import com.google.common.base.Preconditions;
import pl.edu.agh.ctp.SystemPropertiesConstants;

import java.util.Scanner;

/**
 * Created by Leszek Placzkiewicz on 2016-04-26.
 */
public class CommandLineClient {

    private final Scanner scanner;

    private final ClientRequestHandler clientRequestHandler;


    public CommandLineClient(String authenticationUser, String authenticationPassword) {
        Preconditions.checkNotNull(authenticationUser);
        Preconditions.checkNotNull(authenticationPassword);

        this.scanner = new Scanner(System.in);
        this.clientRequestHandler = new ClientRequestHandler(authenticationUser, authenticationPassword);
    }

    public void run() {
        while (true) {
            displayMenu();
            String userInput = readUserInput();
            parseUserInput(userInput);
        }
    }

    private void displayMenu() {
        String menu = "------------------------------------------\n" +
                "cenum\t\tenumerate conferences\n" +
                "clay\t\tset conference layout\n" +
                "penum\t\tenumerate participants\n" +
                "pfecc\t\tcontrol far end camera for participant\n" +
                "pconf\t\tget any of participant configuration parameter\n" +
                "pmsg\t\tsend message to participant\n" +
                "pmod\t\tmodify participant configuration\n" +
                "paud\t\tcontrol audio of participant\n" +
                "pfoc\t\tset participant's focus type\n" +
                "play\t\tset participant's layout\n" +
                "freg\t\tregister for receiving feedback\n" +
                "padd\t\tadd participant to conference\n" +
                "";
        System.out.println(menu);
    }

    private void parseUserInput(String userInput) {
        switch (userInput) {
            case "cenum":
                clientRequestHandler.handleEnumerateConferences();
                break;
            case "clay":
                clientRequestHandler.handleLayoutConference();
                break;
            case "penum":
                clientRequestHandler.handleEnumerateParticipants();
                break;
            case "pfecc":
                clientRequestHandler.handleFeccParticipant();
                break;
            case "pconf":
                clientRequestHandler.handleGetParticipantConfiguration();
                break;
            case "pmsg":
                clientRequestHandler.handleMessageParticipant();
                break;
            case "pmod":
                clientRequestHandler.handleModifyParticipant();
                break;
            case "paud":
                clientRequestHandler.handleMuteParticipant();
                break;
            case "pfoc":
                clientRequestHandler.handleFocusParticipant();
                break;
            case "play":
                clientRequestHandler.handleLayoutParticipant();
                break;
            case "freg":
                clientRequestHandler.handleRegisterFeedback();
                break;
            case "padd":
                clientRequestHandler.handleAddParticipant();
                break;
            default:
                System.out.println("Invalid command try again");
        }
    }

    private String readUserInput() {
        return scanner.nextLine();
    }

    public static void main(String[] args) {
        String authenticationUser = System.getProperty(SystemPropertiesConstants.AUTHENTICATION_USER);
        String authenticationPassword = System.getProperty(SystemPropertiesConstants.AUTHENTICATION_PASSWORD);
        CommandLineClient clc = new CommandLineClient(authenticationUser, authenticationPassword);
        clc.run();
    }
}
