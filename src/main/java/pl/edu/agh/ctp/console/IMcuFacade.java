package pl.edu.agh.ctp.console;

import pl.edu.agh.ctp.common.RequiredParticipantParameters;
import pl.edu.agh.ctp.mcu.enums.Direction;

import java.util.List;
import java.util.Map;

/**
 * Created by Leszek Placzkiewicz on 2016-07-10.
 */
public interface IMcuFacade {

    List<Map<String, Object>> enumerateConferences();

    List<Map<String, Object>> enumerateParticipants();

    void feccParticipant(RequiredParticipantParameters requiredParticipantParameters, Direction direction);

    void sendMessage(RequiredParticipantParameters requiredParticipantParameters, String message);

    void modifyParticipant(RequiredParticipantParameters requiredParticipantParameters, Map<String, Object> optionalParameters);

    void modifyConference(String conferenceName, Map<String, Object> optionalParameters);

    void registerFeedback(String address);

    void addParticipant(String conferenceName, String participantName, String address);

    Map<String, Object> queryConferenceStreaming(String conferenceName);

    void modifyConferenceStreaming(String conferenceName, Map<String, Object> optionalParameters);
}
