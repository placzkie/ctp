package pl.edu.agh.ctp.console;

import com.google.common.base.Preconditions;
import pl.edu.agh.ctp.common.RequiredParticipantParameters;
import pl.edu.agh.ctp.mcu.call_constructors.ConferenceMcuMethodCallConstructor;
import pl.edu.agh.ctp.mcu.call_constructors.FeedbackMcuMethodCallConstructor;
import pl.edu.agh.ctp.mcu.call_constructors.ParticipantMcuMethodCallConstructor;
import pl.edu.agh.ctp.mcu.enums.Direction;
import pl.edu.agh.ctp.mcu.enums.OperationScope;
import pl.edu.agh.ctp.mcu.sender.RequestSender;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Leszek Placzkiewicz on 2016-04-26.
 */
public class McuFacadeRemoteImpl implements IMcuFacade {

    private final ConferenceMcuMethodCallConstructor conferenceMcuMethodCallConstructor;

    private final ParticipantMcuMethodCallConstructor participantMcuMethodCallConstructor;

    private final FeedbackMcuMethodCallConstructor feedbackMcuMethodCallConstructor;


    private McuFacadeRemoteImpl(String authenticationUser, String authenticationPassword, RequestSender requestSender) {
        Preconditions.checkNotNull(authenticationUser);
        Preconditions.checkNotNull(authenticationPassword);
        Preconditions.checkNotNull(requestSender);

        this.conferenceMcuMethodCallConstructor = new ConferenceMcuMethodCallConstructor(authenticationUser,
                authenticationPassword, requestSender);
        this.participantMcuMethodCallConstructor = new ParticipantMcuMethodCallConstructor(authenticationUser,
                authenticationPassword, requestSender);
        this.feedbackMcuMethodCallConstructor = new FeedbackMcuMethodCallConstructor(authenticationUser,
                authenticationPassword, requestSender);
    }


    public void modifyConferenceStreaming(String conferenceName, Map<String, Object> optionalParameters) {
        conferenceMcuMethodCallConstructor.modifyConferenceStreaming(conferenceName, optionalParameters);
    }

    public Map<String, Object> queryConferenceStreaming(String conferenceName) {
        return conferenceMcuMethodCallConstructor.queryConferenceStreaming(conferenceName);
    }

    public List<Map<String, Object>> enumerateConferences() {
        List<Object> conferences = conferenceMcuMethodCallConstructor.enumerateConferences();
        return conferences.stream()
                .map(conference -> (Map<String, Object>) conference)
                .collect(Collectors.toList());
    }

    public List<Map<String, Object>> enumerateParticipants() {
        List<Object> participants = participantMcuMethodCallConstructor.enumerateParticipants();
        return participants.stream()
                .map(participant -> (Map<String, Object>) participant)
                .collect(Collectors.toList());
    }

    public void feccParticipant(RequiredParticipantParameters requiredParticipantParameters, Direction direction) {
        participantMcuMethodCallConstructor.feccParticipant(requiredParticipantParameters, direction);
    }

    public void sendMessage(RequiredParticipantParameters requiredParticipantParameters, String message) {
        participantMcuMethodCallConstructor.messageParticipant(requiredParticipantParameters, message);
    }

    public void modifyParticipant(RequiredParticipantParameters requiredParticipantParameters,
                                  Map<String, Object> optionalParameters) {
        participantMcuMethodCallConstructor.modifyParticipant(requiredParticipantParameters,
                OperationScope.ACTIVE_STATE, optionalParameters);
    }

    public void modifyConference(String conferenceName, Map<String, Object> optionalParameters) {
        conferenceMcuMethodCallConstructor.modifyConference(conferenceName, optionalParameters);
    }

    public void registerFeedback(String address) {
        List<Object> receivers = feedbackMcuMethodCallConstructor.queryFeedback();
        System.out.println(receivers);//TODO - remove
//        feedbackMcuMethodCallConstructor.registerFeedback(address);//TODO - uncomment
        receivers = feedbackMcuMethodCallConstructor.queryFeedback();
        System.out.println(receivers);//TODO - remove
    }

    public void addParticipant(String conferenceName, String participantName, String address) {
        participantMcuMethodCallConstructor.addParticipant(conferenceName, participantName, address);
    }
}
