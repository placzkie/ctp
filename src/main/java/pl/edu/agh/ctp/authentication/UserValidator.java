package pl.edu.agh.ctp.authentication;

import com.google.common.base.Preconditions;
import pl.edu.agh.ctp.users.IUserRepository;
import pl.edu.agh.ctp.users.UserCredentials;

/**
 * Created by Leszek Placzkiewicz on 2016-08-07.
 */
public class UserValidator {

    private final IUserRepository userRepository;


    public UserValidator(IUserRepository userRepository) {
        this.userRepository = Preconditions.checkNotNull(userRepository);
    }

    public boolean isValid(UserCredentials userCredentials) {
        return userRepository.authenticateUser(userCredentials);
    }
}
